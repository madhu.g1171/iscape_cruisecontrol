#!/bin/bash

# Simon Jones
#
# Updated 25-09-2006 - moved the nohup to the java command.
# This script will now exit normally after starting the 
# java command in the background.


nohup /home/bob/cc-work/cruisecontrol.sh &
echo "Cruise started. For logs see: tail -100f ./cc.log"

