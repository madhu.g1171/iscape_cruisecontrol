#!/bin/sh

JAVA_HOME=/home/bob/tools/jdk
ANT_HOME=/home/bob/tools/ant

export JAVA_HOME ANT_HOME

$ANT_HOME/bin/ant -lib $ANT_HOME/lib/optional/mail.jar:$ANT_HOME/lib/optional/activation.jar -f ~/cc-work/build-iScape.xml email-stats
