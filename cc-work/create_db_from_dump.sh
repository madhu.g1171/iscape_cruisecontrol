#!/bin/bash

# Script run during the build process to recreate the powersearch database.
. /home/bob/.bash_profile
PATH=$PATH:/home/bob/tools/mysql/bin

DB_DUMP_DIR=/srv/ftp/pub/rpms
WILDCARD=??-??-????-master*
LATEST_DUMP=`ls -t $DB_DUMP_DIR/$WILDCARD | awk '{ RS="\n\n+"; FS="/"; print $1 }'`

echo "Recreating main powersearch database."
cd /home/bob/cc-work/checkout/webdev-$1-$2/global/database/prod/MySQL/scripts/dump
echo "Copying latest master dump from $LATEST_DUMP"
cp $LATEST_DUMP ./master-dump.tar.gz

echo "Resetting MySQL binary logs"
mysql -u root --password=password << EnDsQl
reset master
\g
EnDsQl

echo "loading dump into powersearch-$1-$2"
gzip -dc master-dump.tar.gz | tar -O -xf - > master-dump.sql

sed 's/Current Database: `POWERSEARCH`/Current Database: `POWERSEARCH-'$1-$2'`/' master-dump.sql | sed 's/DROP DATABASE IF EXISTS `POWERSEARCH`/DROP DATABASE IF EXISTS `POWERSEARCH-'$1-$2'`/' | sed 's/CREATE DATABASE \(.*\) `powersearch`/CREATE DATABASE \1 `powersearch-'$1-$2'`/' | sed 's/USE `POWERSEARCH`/USE `POWERSEARCH-'$1-$2'`/' > master-dump-modified.sql

mysql -u root --password=password < master-dump-modified.sql

echo "load complete"

rm master-dump-modified.sql


echo "cleanup complete"
