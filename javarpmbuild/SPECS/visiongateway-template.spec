Summary: The CVI gateway
Name: visiongateway
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/visiongateway
Prefix: /tui/iscape/releases

%description
CVI

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
get /pub/artifacts/visiongateway/@cruisebuilddate@/visiongateway.jar visiongateway.jar
get /pub/artifacts/visiongateway/@cruisebuilddate@/visiongateway-dependencies.zip dependencies.zip
get /pub/artifacts/visiongateway/@cruisebuilddate@/visiongateway.conf visiongateway.conf
get /pub/artifacts/visiongateway/@cruisebuilddate@/wrapper.conf wrapper.conf
get /pub/artifacts/visiongateway/@cruisebuilddate@/visiongateway.sh visiongateway.sh
get /pub/artifacts/visiongateway/@cruisebuilddate@/log4j.properties log4j.properties
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp visiongateway.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
cp dependencies.zip $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
cp visiongateway.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
cp visiongateway.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
cp wrapper.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
cp log4j.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/visiongateway
cp rpmbuild/deployscripts/deployvisiongateway.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/check4redundant_iscape_tags.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
chmod  ug+x /tui/iscape/releases/is@version@.@release@/visiongateway/visiongateway.sh
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT 

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/visiongateway/visiongateway.jar
/tui/iscape/releases/is@version@.@release@/visiongateway/dependencies.zip
/tui/iscape/releases/is@version@.@release@/visiongateway/visiongateway.sh
/tui/iscape/releases/is@version@.@release@/visiongateway/visiongateway.conf
/tui/iscape/releases/is@version@.@release@/visiongateway/wrapper.conf
/tui/iscape/releases/is@version@.@release@/visiongateway/log4j.properties
/tui/iscape/releases/is@version@.@release@/deployscripts/deployvisiongateway.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/check4redundant_iscape_tags.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
