Summary: The groundtrader program
Name: groundtrader
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/groundtrader
Prefix: /tui/iscape/releases

%description
Groundtrader.

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
get /pub/artifacts/groundtrader/@cruisebuilddate@/gt-cps-batch.zip gt-cps-batch.zip
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/publish rpmbuild/publish
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/groundtrader
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp gt-cps-batch.zip $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/groundtrader
cp rpmbuild/deployscripts/deploygroundtrader.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/check4redundant_iscape_tags.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT 

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/groundtrader/gt-cps-batch.zip
/tui/iscape/releases/is@version@.@release@/deployscripts/deploygroundtrader.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/check4redundant_iscape_tags.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
