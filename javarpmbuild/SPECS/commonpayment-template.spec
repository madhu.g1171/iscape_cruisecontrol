Summary: The TUI common payment application.
Name: commonpayment
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI Travel PLC
Packager: TUI Travel PLC
BuildRoot: /var/tmp/commonpayment
Prefix: /tui/iscape/releases

%description
The common payment application is designed to provide a common interface for payment to be taken, including payment pages etc. for PCI compliance.

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
get /pub/artifacts/commonpayment/@cruisebuilddate@/cps.war cps.war
get /pub/artifacts/commonpayment/@cruisebuilddate@/cps.conf cps.conf
get /pub/artifacts/commonpayment/@cruisebuilddate@/datacash.conf datacash.conf
get /pub/artifacts/commonpayment/@cruisebuilddate@/cms-cps.zip cms-cps.zip
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/publish rpmbuild/publish
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp cps.war $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
cp cps.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp datacash.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/tomcat.sh.commonpaymentserver $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
cp rpmbuild/publish/conf/catalina.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/server.xml.commonpaymentserver $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
cp rpmbuild/publish/conf/tomcat-users.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/web.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/iScape/localhost/host-manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/cps.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/logging/classes/log4j.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
cp rpmbuild/publish/logging/lib/commons-logging-1.1.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/publish/logging/lib/log4j-1.2.13.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/deployscripts/deploycommonpaymentserver.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/deploy_cps_groundtrader.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/check4redundant_iscape_tags.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
# Include the groundtrader zipfile (transport via deployscripts dir)
##cp /home/bob/newrpmbuild/work/gt-cps.zip $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT 

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/cps.war
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/cps.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/datacash.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/catalina.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/tomcat-users.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/web.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/host-manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/cps.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes/log4j.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/commons-logging-1.1.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/log4j-1.2.13.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
/tui/iscape/releases/is@version@.@release@/deployscripts/deploycommonpaymentserver.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/deploy_cps_groundtrader.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/check4redundant_iscape_tags.sh
##/tui/iscape/releases/is@version@.@release@/deployscripts/gt-cps.zip

%changelog
* @rpmbuilddate@ Bob Builder <Richard_Hands@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
