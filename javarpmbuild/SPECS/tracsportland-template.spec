Summary: The TUI tracs service portland application.
Name: tracsportland
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI Travel PLC
Packager: TUI Travel PLC
BuildRoot: /var/tmp/tracsportland
Prefix: /tui/iscape/releases

# @(#) last changed by: $Author: chapman $ $Revision: 62 $ $Date: 2008-10-22 12:13:41 +0100 (Wed, 22 Oct 2008) $
# $HeadURL: http://src-repo/svn/newrpmbuild/trunk/SPECS/tracsportland-template.spec $

%description
The Tracs Service application supporting the portland tracs messages.

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
get /pub/artifacts/tracs_service_portland/@cruisebuilddate@/TRACSPORTLANDSEARCH.war TRACSPORTLANDSEARCH.war
get /pub/artifacts/tracs_service_portland/@cruisebuilddate@/TRACSPORTLANDBOOK.war TRACSPORTLANDBOOK.war
get /pub/artifacts/tracs_service_portland/@cruisebuilddate@/portlandtracsservice.conf portlandtracsservice.conf
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/publish rpmbuild/publish
svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

cp /srv/ftp/pub/artifacts/tracs_service_portland/svn-portland_tracs_service_ts2_00_11/TRACSPORTLAND*.war $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts


cp portlandtracsservice.conf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/tomcat.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
cp rpmbuild/publish/conf/catalina.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/server.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
cp rpmbuild/publish/conf/tomcat-users.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/web.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf
cp rpmbuild/publish/conf/iScape/localhost/host-manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/conf/iScape/localhost/manager.xml $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost
cp rpmbuild/publish/logging/classes/log4j.properties $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes
cp rpmbuild/publish/logging/lib/commons-logging-1.1.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/publish/logging/lib/log4j-1.2.13.jar $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib
cp rpmbuild/deployscripts/deploytracsserviceportland.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/check4redundant_iscape_tags.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
# xslt
cd /home/bob/newrpmbuild/releasecheckout/tracs_service_portland/src/conf/xslt
tar -cf $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts/tsp_xslt_files.tar *.xsl PriceTypeLookUp.xml




%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT 

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/TRACSPORTLANDSEARCH.war
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/artifacts/TRACSPORTLANDBOOK.war
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/portlandtracsservice.conf
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/tomcat.sh
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/catalina.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/server.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/tomcat-users.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/web.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/host-manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/conf/iScape/localhost/manager.xml
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/classes/log4j.properties
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/commons-logging-1.1.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logging/lib/log4j-1.2.13.jar
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/logs
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/temp
/tui/iscape/releases/is@version@.@release@/rpmiscape/publish/webapps
/tui/iscape/releases/is@version@.@release@/deployscripts/deploytracsserviceportland.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/check4redundant_iscape_tags.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/tsp_xslt_files.tar

%changelog
* @rpmbuilddate@ Bob Builder <Richard_Hands@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
