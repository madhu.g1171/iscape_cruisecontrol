Summary: The xDistributor component for thomson on iScape
Name: xdist
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/thomson
Prefix: /tui/iscape/releases

%description
xDistributor provides aggregation and switching middleware with a business rules engine.

%prep
# get the binary artifacts from the build machine
ftp -i -v -n localhost << bye
user "ftp" "rpmbuild"
binary
get /pub/artifacts/xdist/@cruisebuilddate@/xdist-tokenized.zip xdist-tokenized.zip
bye

svn --force export http://10.145.36.205/svn/iscape_cruisecontrol/trunk/rpmbuild/deployscripts rpmbuild/deployscripts

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/xdist
mkdir -p $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp xdist-tokenized.zip $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/xdist
cp rpmbuild/deployscripts/deployxdist.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts
cp rpmbuild/deployscripts/editproperties.sh $RPM_BUILD_ROOT/tui/iscape/releases/is@version@.@release@/deployscripts

%post
chown -R iscape:ttguser /tui/iscape/releases/is@version@.@release@
chmod -R g+w /tui/iscape/releases/is@version@.@release@
echo "Install completed."

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,iscape,ttguser)

/tui/iscape/releases/is@version@.@release@/xdist/xdist-tokenized.zip
/tui/iscape/releases/is@version@.@release@/deployscripts/deployxdist.sh
/tui/iscape/releases/is@version@.@release@/deployscripts/editproperties.sh

%changelog
* @rpmbuilddate@ Bob Builder <Julia_Dain@tui-uk.co.uk>
- Build info: RPM build date @rpmbuilddatetime@ / 
- Built from: @cruisebuildlabel@ / @cruisebuilddate@
