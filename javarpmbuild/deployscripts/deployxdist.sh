#!/bin/bash
#
# Post-install script to deploy xDistributor 
#   as used by javarpmbuild (i.e. NOT the old rpmbuild)
# Usage: deployxdist.sh release_name target_name log4j.properties [logfile]
# Note: target_name must end with either a, b, c, d or e, indicating which xdist instance is being deployed
#
# Examples: deployxdist.sh is01.00.01 iscapet1a /tui/iscape/config/xdist-runtime.properties /tui/iscape/config/xdist-log4j.properties

# set up the environment
TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh
export JAVA_HOME=$JAVA14
export TOMCAT_HOME=$TOMCAT5_HOME

usage="Usage: $0 release_name target_name xdist_props_path log4j_props_path [logfile_path]"

# New release dir is specified as first arg, target deployment directory as second
RELEASE_NAME=$1
TARGET_NAME=$2
XDIST_PROPS=$3
LOG4J_PROPS=$4

if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage
    exit 1
fi
if [ ! -f "$XDIST_PROPS" ]
then
    echo $usage
    echo "$XDIST_PROPS is not a readable file"
    exit 2
fi
if [ ! -f "$LOG4J_PROPS" ]
then
    echo $usage
    echo "$LOG4J_PROPS is not a readable file"
    exit 3
fi
if [ ! -x "$ANT" ]
then
    echo "ANT environment variable not set correctly"
    echo "Please check /tui/iscape/program/setenv.sh"
    exit 4
fi

# Optional fifth argument is log file. If not specified
# then output goes to stdout
if [ $# = 5 ]; then
    LOGFILE="$5"
fi

# Check TARGET_NAME ends with an instance specifier
ISCAPE_INSTANCE=`echo $TARGET_NAME | sed -e "s/.*\(.\)$/\1/g"`
if [ `echo $ISCAPE_INSTANCE | sed s/[a-g]//1` ]
then
        echo "ERROR: Second parameter must end with either a, b, c, d, e, f or g, indicating which xdist instance is being deployed"
        exit 7 
fi

# Check required environment variables have been set (by setenv.sh)
xdist_http_port_name=xdist_http_port_instance_${ISCAPE_INSTANCE}
xdist_http_port=${!xdist_http_port_name}
xdist_shutdown_port_name=xdist_shutdown_port_instance_${ISCAPE_INSTANCE}
xdist_shutdown_port=${!xdist_shutdown_port_name}
xdist_jmx_port_name=xdist_jmx_port_instance_${ISCAPE_INSTANCE}
xdist_jmx_port=${!xdist_jmx_port_name}
#echo "debug: xdist_http_port is $xdist_http_port   xdist_shutdown_port is $xdist_shutdown_port    xdist_jmx_port is $xdist_jmx_port   - for instance $ISCAPE_INSTANCE"
if [ "$xdist_http_port" = "" ]
then
    echo "Environment variable ${xdist_http_port_name} does not exist. (should probably be set in program/setenv.sh).   #####  ABORTING.  #####"
    exit 8
fi
if [ "$xdist_shutdown_port" = "" ]
then
    echo "Environment variable ${xdist_shutdown_port_name} does not exist. (should probably be set in program/setenv.sh).   #####  ABORTING.  #####"
    exit 8
fi
if [ "$xdist_jmx_port" = "" ]
then
    echo "Environment variable ${xdist_jmx_port_name} does not exist. (should probably be set in program/setenv.sh).   #####  ABORTING.  #####"
    exit 8
fi


# Checks complete. Start processing
echo "Deploying xDist instance $ISCAPE_INSTANCE to this host (`hostname`)"



RELEASE_DIR=$TUI_ISCAPE/releases/$RELEASE_NAME
cd $RELEASE_DIR

if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 

# The location into which the webapp will be deployed. The ant detokenizing process will create this directory.
XDIST_DIR="`pwd`/xdist"

# a temporary location into which to unpack the release
TEMP_DIR="`pwd`/tmp"
if [ ! -d "$TEMP_DIR" ]
then
    mkdir -p "$TEMP_DIR"
fi

# unpack the xDist release into the temp directory, ready for detokenizing
echo -n "Unpacking xDist..." | tee -a $LOGFILE
unzip -o -d "$TEMP_DIR" "$RELEASE_DIR/xdist/xdist-tokenized.zip" | tee -a $LOGFILE >/dev/null

# detokenize the xDist release - this will create and populate the deployment directory $XDIST_DIR
echo -n "Detokenizing..." | tee -a $LOGFILE
cd $TEMP_DIR 
if [ $# = 5 ]; then
    ANTLOGFILE="$5"
  else
    ANTLOGFILE="/dev/null"
fi
$ANT -Dproperties="$XDIST_PROPS" -DTARGET_PATH="$XDIST_DIR" detokenise >>$ANTLOGFILE || exit 5
cd $XDIST_DIR
rm -rf $TEMP_DIR


# copy the Oracle jdbc jar from the Oracle install dir, if it exists
oraclejar=ojdbc14.jar
oracledeploydir=$XDIST_DIR/publish/tomcat5.0/oracle
if test -f $ORACLE9I_CLIENT_BIN/$oraclejar
  then
    mv $oracledeploydir/$oraclejar $oracledeploydir/$oraclejar.bak
    cp $ORACLE9I_CLIENT_BIN/$oraclejar $oracledeploydir
  else
    echo "Warning - $ORACLE9I_CLIENT_BIN/$oraclejar not found." \
          "Using version from release rpm instead."
fi

# copy the amadeus.jar from the AMADEUS_API_HOME, if it exists
shared=$XDIST_DIR/publish/tomcat5.0/shared/lib
if test -f $AMADEUS_API_HOME/amadeus.jar
  then
    mv $shared/amadeus.jar $shared/amadeus.jar.bak
    cp $AMADEUS_API_HOME/amadeus.jar $shared
  else
    echo "Warning - $AMADEUS_API_HOME/amadeus.jar not found." \
           "Using version from release rpm instead."
fi

# compile the rules
echo "Compiling xDist rules, please be patient..."
$ANT compile-xdist-rules >>$ANTLOGFILE || exit 6 

# edit log4j.properties
echo -n "Editing log4j.properties..." | tee -a $LOGFILE
cd $RELEASE_DIR
deployscripts/editproperties.sh $XDIST_DIR/publish/tomcat5.0/shared/classes/log4j.properties $LOG4J_PROPS

chmod -R g+rw $XDIST_DIR

echo " "
echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE



# Post-deployment step 1
echo " Post-deployment step 1 - in server.xml and tomcat5.0.sh, adjust port numbers depending upon instance number" | tee -a $LOGFILE
SERVERXMLDIR=${RELEASE_DIR}/${TARGET_NAME}/xdist/publish/tomcat5.0/conf
TOMCATSHDIR=${RELEASE_DIR}/${TARGET_NAME}/xdist/publish

# Substitute the http & shutdown port numbers within server.xml
echo "    substituting port numbers within server.xml"
mv ${SERVERXMLDIR}/server.xml ${SERVERXMLDIR}/server.xml.org
cat ${SERVERXMLDIR}/server.xml.org \
  | sed -e "s/\(<Server port=\"\)[0-9]*\"/\1${xdist_shutdown_port}\"/" \
  | sed -e "s/\(<Connector port=\"\)9090\"/\1${xdist_http_port}\"/" \
  > ${SERVERXMLDIR}/server.xml

# Substitute the jmx port number within tomcat5.0.sh
echo "    substituting jmx port number within tomcat5.0.sh"
mv ${TOMCATSHDIR}/tomcat5.0.sh ${TOMCATSHDIR}/tomcat5.0.sh.org
cat ${TOMCATSHDIR}/tomcat5.0.sh.org \
  | sed -e "s/\(com.sun.management.jmxremote.port=\)[0-9]*/\1${xdist_jmx_port}/" \
  > ${TOMCATSHDIR}/tomcat5.0.sh
chmod u+x ${TOMCATSHDIR}/tomcat5.0.sh

echo " Post-deployment step 1 completed." | tee -a $LOGFILE



echo "Done." | tee -a $LOGFILE



