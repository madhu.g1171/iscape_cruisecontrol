#!/bin/sh
#
# Post-install script to deploy the brac webapp
#   as used by javarpmbuild (i.e. NOT the old rpmbuild)
# Usage: sh deploybrac.sh release_name target_name brac.properties [logfile]
#
# Examples: deploybrac.sh is01.01.01 iscapet1 brac-iscapet1.properties powersearch.xsl
#
# Assumes the brac rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the brac artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#

# History follows
# 28/03/07 A Chapman     Add post-deployment step section + step to create softlink to ojdbc14.jar such that BRAC logins are possible
# 03/07/07 A Chapman     Add 2nd post-deployment step : Insert brac login stanza into server.xml
# 30/07/07 A Chapman     Check for unused substitution values
# 27/02/08 A Chapman     Bugfix. Issues remain (inc only works for 'config' module (not sat/dev))
#
#
#
# @(#) last changed by: $Author: chapman $ $Revision: 1.2 $ $Date: 2008/02/27 17:22:04 $
# $Source: /var/cvsdata/newdev/newrpmbuild/deployscripts/deploybrac.sh,v $

TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh

usage="Usage: $0 release_name target_name brac_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
brac_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi

if [ ! -f "$brac_PROPS" -o ! -r "$brac_PROPS" ]; then
    echo "Couldn't open brac config file $brac_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi

echo -n "Deploying brac..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $DEPLOY_DIR/conf/brac.conf $brac_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $DEPLOY_DIR/conf/brac.conf $brac_PROPS
chmod g+r $DEPLOY_DIR/conf/brac.conf


echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE



# 1st post-deployment step - create softlink to ojdbc14.jar such that BRAC logins are possible
if mkdir -p $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib
then
  cd $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib
  ln -s /tui/iscape/program/instantclient_10_2_sparc_32/ojdbc14.jar ojdbc14.jar
fi
if [ ! -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib/ojdbc14.jar ]
then
  echo "Post-Deploy step failed. Unable to softlink from $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib/ojdbc14.jar to /tui/iscape/program/instantclient_10_2_sparc_32/ojdbc14.jar" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE



# 2nd post-deployment step - Insert brac login stanza into server.xml
thishostenv=`hostname | cut -b-3`
deploy_security_stanza=true
case "${thishostenv}" in
  ukt) 
    db_pw_end3="q12"
    configdir="wish_pat"
    ;;
  ukp) 
    db_pw_end3="hma"
    configdir="wish_prod"
    ;;
  *) 
    db_pw_end3=""
    echo "ERROR: Unexpected environment of ${thishostenv} in $0." | tee -a $LOGFILE
    echo " Hence 2nd Post-depoly step will not occur - Insert brac login stanza into server.xml" | tee -a $LOGFILE
    deploy_security_stanza=false
esac
if [ "$deploy_security_stanza" = "true" ]
then
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 2 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT BRAC SECURITY STANZA HERE -->\)~\1`cat /tui/iscape/config/config/${configdir}/brac_login.fragment`~" | \
        sed -e 's/@CRLF@/\
/g' | sed -e "s/@DB_PW_TAIL@/${db_pw_end3}/" > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
  echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE
fi

echo "Done." | tee -a $LOGFILE
