#!/bin/sh
#
# Post-install script to deploy an iscape webapp
#   as used by javarpmbuild (i.e. NOT the old rpmbuild)
# Usage: sh deploywebapp.sh project_name release_name target_name thomson.properties [logfile]
#
# Example: deploywebapp.sh thomson is01.01.01 iscapet1 thomson-iscapet1.properties powersearch.xsl
#
# Assumes the webapp project rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the webapp artifacts (.war and .conf) into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#

TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh

usage="Usage: $0 project_name release_name target_name thomson_config_path [logfile_path]"

project=$1
shift 

RELEASE_NAME=$1
TARGET_NAME=$2
WEBAPP_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$WEBAPP_PROPS" -o ! -r "$WEBAPP_PROPS" ]; then
    echo "Couldn't open $project config file $WEBAPP_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi

echo -n "Deploying $project..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
DEPLOY_DIR="`pwd`/publish"

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $DEPLOY_DIR/conf/$project.conf $WEBAPP_PROPS
chmod g+r $DEPLOY_DIR/conf/$project.conf

echo "Done." | tee -a $LOGFILE
