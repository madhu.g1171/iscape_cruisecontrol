#!/bin/sh
#
# Create a distribution package for the Amadeus libraries and OpenJaw bridge and jar for xDistributor.
#
# WARNING - assumes intimate knowledge of what's in CVS under xdistdev/xdist/amadeus.
#
# SYNOPSIS
#    amadeuszipbuild.sh [ linux | solaris ]
#
# AUTHOR
#    Julia Dain, TUI UK, November 2006

usage="USAGE: $0 [ linux | solaris ]"

# check arguments
arch=$1
case $arch in
  linux) libname=Linux2-6 ;;
  solaris) libname=SunOS8 ;;
  *) echo $usage ; exit 1 ;;
esac

# create a temporary directory to do stuff in
tmpdirname=tmp`echo $$`
mkdir $tmpdirname
cd $tmpdirname
tmpdir=`pwd`
  
# check out Amadeus libraries  
cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development co xdistdev/xdist/amadeus/${arch}

cd xdistdev/xdist/amadeus/${arch}

gunzip API_2_1_32_0.${libname}_32.tar.gz
tar xf API_2_1_32_0.${libname}_32.tar
cd API_2_1_32_0.${libname}_32/Proxy/lib/gcc/
chmod u+w .

cp ../../../../amadeus.jar .
cp ../../../../libAmadeus.so .

# tar and zip everything up
tar cf ${tmpdir}/amadeus.tar *
gzip ${tmpdir}/amadeus.tar

echo "******************************************************************"
echo "   PLEASE READ"
echo "${tmpdir} contains a distribution file amadeus.tar.gz"
echo "containing the Amadeus native libraries v32 plus libAmadeus.so and amadeus.jar."
echo "This file should be copied to the Linux/Solaris target machine"
echo "and unpacked into the AMADEUS_API_HOME."
echo "Please see the iScape wiki for more info."
echo "******************************************************************"

