Summary: A dummy package to test rpm build and install
Name: testrpm
Version: @version@
Release: @release@
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/thomson
Prefix: /tui/iscape/testing
# example Requires tag
# Requires: tomcat

%description
testrpm contains a few files to test package build and install

%prep
cp /home/bob/cc-work/artifacts/latest/conf/thomson.conf .
cp /home/juliad/rpmtest/file1 .
mkdir -p utils
cp /home/juliad/rpmtest/testme.sh utils

%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tui/iscape/testing/is@version@.@release@
mkdir -p $RPM_BUILD_ROOT/tui/iscape/testing/is@version@.@release@/utils
cp thomson.conf $RPM_BUILD_ROOT/tui/iscape/testing/is@version@.@release@
cp file1 $RPM_BUILD_ROOT/tui/iscape/testing/is@version@.@release@
cp utils/testme.sh $RPM_BUILD_ROOT/tui/iscape/testing/is@version@.@release@/utils

%post
/tui/iscape/testing/is@version@.@release@/utils/testme.sh

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)

/tui/iscape/testing/is@version@.@release@/thomson.conf
/tui/iscape/testing/is@version@.@release@/file1
/tui/iscape/testing/is@version@.@release@/utils/testme.sh

%changelog
* @rpmbuilddate@ Julia Dain <Julia_Dain@tui-uk.co.uk>
- build date @rpmbuilddatetime@ / CruiseControl date @cruisebuilddate@

* Thu Sep 28 2006 Julia Dain <Julia_Dain@tui-uk.co.uk>
- templated the spec for version and release parameterization

* Wed Sep 27 2006 Julia Dain <Julia_Dain@tui-uk.co.uk> 
- added script file to package, and its execution to post section

* Tue Sep 19 2006 Julia Dain <Julia_Dain@tui-uk.co.uk> 
- first draft of RPM spec

