Summary: A virtual package for /bin/bash
Name: bash
Version: 01.01
Release: 01
License: commercial
Group: Applications/Internet
Vendor: TUI UK
Packager: TUI UK
BuildRoot: /var/tmp/thomson
Prefix: /tui/iscape/current
Provides: /bin/bash

%description
A virtual package for /bin/bash

%prep

%build

%install

%clean

%files


%changelog
* Fri Oct 20 2006 Julia Dain <Julia_Dain@tui-uk.co.uk> 
- first draft 
