#!/bin/sh
#
# Script to create a database zip from cvs label
#
# USAGE
#        dbzipbuild.sh VERSION RELEASE  CVSLABEL
# VERSION should be in the approved TUI format mm.nn where mm gives the major version and nn the minor.
# RELEASE should be in the approved TUI format rr where rr gives the release (build) number for the version.
# CVSLABEL is a CVS revision tag (label) specifying which CVS sources will be checked out and zipped up.
#
# EXAMPLE
#        ./dbzipbuild.sh 01.00 02 is01_00_02
# AUTHOR
#        Julia Dain, TUI UK

version=$1
release=$2
cvslabel=$3

if [ -z "$version" -o -z "$release" -o -z "$cvslabel" ]; then  
  echo "USAGE: $0 VERSION RELEASE CVSLABEL" ; exit 1
fi

cd releasecheckout

# checkout DB files
cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -r $cvslabel webdev/global/database/prod/MySQL/scripts/dump/README-MasterDump.TXT  || exit 2
cvs -d:pserver:cvsaccess:cvshost@tuicovcvs:/var/cvsdata/development update -r $cvslabel webdev/global/database/prod/MySQL/scripts/dump/master-dump.tar.gz || exit 3

# zip them up 
zipfile=`pwd`/db-$1-$2.zip
cd webdev/global/database/dev/MySQL/scripts/backups
zip $zipfile README-MasterDump.TXT master-dump.tar.gz || exit 4

# move zip to ftp area
ftp=/srv/ftp/pub/rpms
echo "Moving" db-$1-$2.zip "to" $ftp
mv $zipfile $ftp
