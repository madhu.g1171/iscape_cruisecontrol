############################################################################
#
# deploywss.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the wss webapp
# Usage: sh deploywss.sh release_name target_name wss.properties [logfile]
#
# Examples: deploywss.sh wss01.00.00 iscapet1 wss-iscapet1.properties 
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for wss deployments. 
#
# History follows
# 13/07/09 Ramesh Babu   Created the file to deploy wss application.
# 18/09/09 Ramesh Babu   Included WSS PAT and PRD servers for i3 support.
# 01/10/09 Ramesh Babu   Updated routingfilepath parameter to pick iscape_routing file as per TS and CPS deployments.
# 02/11/10 Ramesh Babu   Removed PAT servers list from Precise servers list section as per discussion with Chris on 2/11/2010.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
#
############################################################################


DEPLOYWSS_SH_TEMPLATE=1
deploywss_sh_cvsrev="$Revision$"
DEPLOYWSS_SH_REVISION=`echo $deploywss_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name wss_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
WSS_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$WSS_PROPS" -o ! -r "$WSS_PROPS" ]; then
    echo "Couldn't open wss config file $WSS_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"




echo -n "Deploying wss..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
WSS_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $WSS_DEPLOY_DIR/conf/wss.conf $WSS_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $WSS_DEPLOY_DIR/conf/wss.conf $WSS_PROPS
chmod g+r $WSS_DEPLOY_DIR/conf/wss.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYWSS_SH_TEMPLATE=$DEPLOYWSS_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYWSS_SH_CVSREVISION=$DEPLOYWSS_SH_REVISION" >> $manifestfile
echo "WSS-ISCAPE_PROPERTIES_TAGGED_WSS_TEMPLATE="`cat $WSS_PROPS | grep "^WSS_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "ukpwap07" \
  -a "$thishost" != "ukpwap01" \
  -a "$thishost" != "ukpwap02" \
  -a "$thishost" != "ukpwap06" \
  -a "$thishost" != "ukpwap30" \
  -a "$thishost" != "ukpwap31" \
  -a "$thishost" != "ukpwap60" \
  -a "$thishost" != "ukpwap61" \
  ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar \
  -a ! -r /tui/iscape/program/tomcat5.0/jakarta-tomcat-5.0.28/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED" | tee -a $LOGFILE
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE



# 2nd post-deployment step - As appropriate, copy the wss_datacash.conf from cvs
brnd=`echo $RELEASE_NAME | cut -b3-5`
dcname=wss_datacash.conf
# Determine cvs directory
cvs_datacash_filespec=`echo $WSS_PROPS | sed "s:[^/]*/[^/]*$:${brnd}/${dcname}:"`
dev_datacash_filespec=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcname
#echo "DEBUG: cvs_datacash_filespec is $cvs_datacash_filespec     dev_datacash_filespec is $dev_datacash_filespec"
if [ -r $cvs_datacash_filespec ]
then
  if [ -r $dev_datacash_filespec ]
  then
    mv $dev_datacash_filespec ${dev_datacash_filespec}_fromdev
    cp -p $cvs_datacash_filespec $dev_datacash_filespec
    echo "  $dcname from cvs for this environment has been deployed."
  else
    cp -p $cvs_datacash_filespec $dev_datacash_filespec
    echo "  INFO: $dcname from CVS has been deployed, but this release seems not to need it."
  fi
else
  if [ -r $dev_datacash_filespec ]
  then
    echo " "
    echo "  *********************************************************************************************"
    echo "  WARNING: This build appears to require a $dcname, yet no such file is provided from CVS"
    echo "  *********************************************************************************************"
    echo " "
  fi
fi

echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE

# 3rd post-deployment step - As appropriate, copy the wss_brand.conf from cvs
brndname=wss_brand.conf
# Determine cvs directory
cvs_brand_filespec=`echo $WSS_PROPS | sed "s:[^/]*/[^/]*$:${brnd}/${brndname}:"`
dev_brand_filespec=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$brndname
#echo "DEBUG: cvs_brand_filespec is $cvs_brand_filespec dev_brand_filespec is $dev_brand_filespec"
if [ -r $cvs_brand_filespec ]
then
  if [ -r $dev_brand_filespec ]
  then
    mv $dev_brand_filespec ${dev_brand_filespec}_fromdev
    cp -p $cvs_brand_filespec $dev_brand_filespec
    echo "  $brndname from cvs for this environment has been deployed."
  else
    cp -p $cvs_brand_filespec $dev_brand_filespec
    echo "  INFO: $brndname from CVS has been deployed, but this release seems not to need it."
  fi
else
  if [ -r $dev_brand_filespec ]
  then
    echo " "
    echo "  *********************************************************************************************"
    echo "  WARNING: This build appears to require a $brndname , yet no such file is provided from CVS"
    echo "  *********************************************************************************************"
    echo " "
  fi
fi

echo "Post-Deployment Step 3 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
