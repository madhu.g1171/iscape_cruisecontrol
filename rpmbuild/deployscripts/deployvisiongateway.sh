#!/bin/sh

# issues:
############
# OK: where logfile go
# OK: how monitor is running (ck)
# OK: get it to stop ^& report status
# OK: port numbers used (31999, 3269 etc.) should these be configurable?
# the many exceptions seen in the onscreen output when running in console
# the 'unchecked call' during the build






############################################################################
#
# deployvisiongateway.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# script to deploy the visiongateway webapp
# Usage: sh deployvisiongateway.sh release_name target_name visiongateway.properties [logfile]
#
# Examples: deployvisiongateway.sh is01.01.01 iscapet1 visiongateway-iscapet1.properties
#
# Assumes the visiongateway rpm has already been rpm-installed
# into /tui/iscape/releases/release_name.
# Deploys the visiongateway artifacts into the deployment directory
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Note: Visiongateway uses MULE  (NOT TOMCAT), 
#  Nontheless, from the deploy and user points-of-view, it deploys and is 
#  monitored/stopped/started in the usual way, providing maximum familiarity.
#
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: MASTER version of this file is held in: {see HeadURL below}
#  upon build, it SHOULD automatically overwrite into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
#
#
# History follows
# 23/01/09 A Chapman     Created, based on fcfalcon
# 10/02/09 A Chapman     Also copy wrapper.conf info /conf + dos2unix the .conf file
# 18/02/09 A Chapman     Tidy
# 09/07/09 A Chapman     Copy the log4j.properties file to the conf directory
# 18/09/09 Ramesh Babu   Added step to create validationExceptions folder under visiongateway folder.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################

DEPLOYVISIONGATEWAY_SH_TEMPLATE=1
deployvisiongateway_sh_cvsrev="$Revision$"
DEPLOYVISIONGATEWAY_SH_REVISION=`echo $deployvisiongateway_sh_cvsrev | cut -d" " -f2`

apply_dos2unix()
{
  # removes any ^M line endings from the specified file. 
  # only parameter specifies file to clean. Can be absolute/relative/no path.
  # Beware: dos2unix behaves differently on solaris & unix. Failsafe.
  echo " Performing dos2unix on file: $1"
  ostype=`uname`
  if [ "$ostype" = "SunOS" ]
  then
    mv $1 ${1}_preTounix
    dos2unix ${1}_preTounix > $1 2>/dev/null
    # rm ${1}_preTounix
  else
    dos2unix $1 > /dev/null 2>/dev/null
  fi
}



# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`


usage="Usage: $0 release_name target_name visiongateway_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
VISIONGATEWAY_PROPS=$3




# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage
    exit 1
fi
if [ ! -f "$VISIONGATEWAY_PROPS" -o ! -r "$VISIONGATEWAY_PROPS" ]; then
    echo "Couldn't open visiongateway config file $VISIONGATEWAY_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/$LOCAL_ISCAPE_ENVIRONMENT"




echo "Deploying visiongateway...    (Release $RELEASE_NAME   Instance $TARGET_NAME)"  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME

# Create the template INSTANCE directory from the release template
mkdir -p $TARGET_NAME
cd $TARGET_NAME


# create & populate the required directory structure
VISIONGATEWAY_DEPLOY_DIR=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/visiongateway
mkdir -p $VISIONGATEWAY_DEPLOY_DIR/lib
mkdir -p $VISIONGATEWAY_DEPLOY_DIR/logs
mkdir -p $VISIONGATEWAY_DEPLOY_DIR/conf
mkdir -p $VISIONGATEWAY_DEPLOY_DIR/logs
mkdir -p $VISIONGATEWAY_DEPLOY_DIR/validationExceptions

cd $VISIONGATEWAY_DEPLOY_DIR/lib
unzip $TUI_ISCAPE/releases/$RELEASE_NAME/visiongateway/dependencies.zip
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visiongateway/visiongateway.jar $VISIONGATEWAY_DEPLOY_DIR/lib
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visiongateway/visiongateway.conf $VISIONGATEWAY_DEPLOY_DIR/conf
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visiongateway/wrapper.conf $VISIONGATEWAY_DEPLOY_DIR/conf
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visiongateway/visiongateway.sh $VISIONGATEWAY_DEPLOY_DIR
cp $TUI_ISCAPE/releases/$RELEASE_NAME/visiongateway/log4j.properties $VISIONGATEWAY_DEPLOY_DIR/conf

apply_dos2unix $VISIONGATEWAY_DEPLOY_DIR/visiongateway.sh
chmod ug+x $VISIONGATEWAY_DEPLOY_DIR/visiongateway.sh
# above done by SPEC, but lost on dos2unix. 

# Now redundant, as check4redundant duz this # apply_dos2unix $VISIONGATEWAY_DEPLOY_DIR/conf/visiongateway.conf

# Now redundant? Helpful signpost to logfiles
#touch $VISIONGATEWAY_DEPLOY_DIR/logs/logs_in___tui_iscape_program_mule_community_2.1.2_MODIFIED_mule-2.1.2_logs

apply_dos2unix $VISIONGATEWAY_DEPLOY_DIR/conf/log4j.properties



echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $VISIONGATEWAY_DEPLOY_DIR/conf/visiongateway.conf $VISIONGATEWAY_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $VISIONGATEWAY_DEPLOY_DIR/conf/visiongateway.conf $VISIONGATEWAY_PROPS
chmod g+r $VISIONGATEWAY_DEPLOY_DIR/conf/visiongateway.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYVISIONGATEWAY_SH_TEMPLATE=$DEPLOYVISIONGATEWAY_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYVISIONGATEWAY_SH_CVSREVISION=$DEPLOYVISIONGATEWAY_SH_REVISION" >> $manifestfile
echo "VISIONGATEWAY-ISCAPE_PROPERTIES_TAGGED_VISIONGATEWAY_TEMPLATE="`cat $VISIONGATEWAY_PROPS | grep "^VISIONGATEWAY_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# 1st post-deployment step - Not required


echo "Done." | tee -a $LOGFILE

