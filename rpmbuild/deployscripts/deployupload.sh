#!/bin/sh

############################################################################
#
# deployupload.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the upload webapp
# Usage: sh deployupload.sh release_name target_name upload.properties [logfile]
#
# Examples: deployupload.sh upload01.00.00 iscapet1 upload-iscapet1.properties 
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for upload deployments. 
#
# History follows
# 01/10/09 Ramesh Babu   Created the file to deploy upload application.
# 05/10/09 Ramesh Babu   Disabled i3 support scripting.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
#
############################################################################

DEPLOYUPLOAD_SH_TEMPLATE=1
deployupload_sh_cvsrev="$Revision$"
DEPLOYUPLOAD_SH_REVISION=`echo $deployupload_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name upload _config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
UPLOAD_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$UPLOAD_PROPS" -o ! -r "$UPLOAD_PROPS" ]; then
    echo "Couldn't open upload config file $UPLOAD_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"


echo -n "Deploying upload ..."  | tee -a $LOGFILE

# For tomcat5*, check that the EL jar files have been added to the tomcat install directories
if [ "$TOMCAT55" != "" ]
then
  if [ ! -f $TOMCAT55/common/lib/el-api-*.jar -o ! -f $TOMCAT55/common/lib/el-impl-*.jar ]
  then
    echo ""
    echo "ERROR: MISSING JARs"
    echo "       For Tomcat5*, upload requires el-api-*.jar & el-impl-*.jar to exist in :"
    echo "         $TOMCAT55/common/lib"
    echo "These files are missing"
    echo "They can be procured from the build machine, from ~bob/ivy-repo"
    echo "with checksums (on solaris) using 'sum el-*.jar':"
    echo "   14057 58 ivy-repo/el-api-1.0.jar"
    echo "   64130 141 ivy-repo/el-impl-1.0.jar"
    echo ""
    echo "Now ABORTING. Please re-run this deployscript once the jars are present."
    echo ""
    echo ""
    exit 1
  fi
fi

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
UPLOAD_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $UPLOAD_DEPLOY_DIR/conf/upload.conf $UPLOAD_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $UPLOAD_DEPLOY_DIR/conf/upload.conf $UPLOAD_PROPS
chmod g+r $UPLOAD_DEPLOY_DIR/conf/upload.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYUPLOAD_SH_TEMPLATE=$DEPLOYUPLOAD_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYUPLOAD_SH_CVSREVISION=$DEPLOYUPLOAD_SH_REVISION" >> $manifestfile
echo "UPLOAD-ISCAPE_PROPERTIES_TAGGED_UPLOAD_TEMPLATE="`cat $UPLOAD_PROPS | grep "^UPLOAD_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp



echo "Deployment performed. No Post-Deployment Step(s)" | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
