#!/bin/sh

######################################################################################################
#
# Post-install script to deploy the brac webapp
#   as used by rpmbuild (i.e. NOT javarpmbuild)
# Usage: sh deploybrac.sh release_name target_name brac.properties [logfile]
#
# Examples: deploybrac.sh is01.01.01 iscapet1 brac-iscapet1.properties powersearch.xsl
#
# Assumes the brac rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the brac artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
#
# History follows
# 28/03/07 A Chapman     Add post-deployment step section + step to create softlink to ojdbc14.jar such that BRAC logins are possible
# 03/07/07 A Chapman     Add 2nd post-deployment step : Insert brac login stanza into server.xml
# 30/07/07 A Chapman     Check for unused substitution values
# 09/04/07 A Chapman     Remove inject-securityRealm-into-server.xml
# 15/04/09 A Chapman     Support [brac_]datacash.conf
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
######################################################################################################

TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh

usage="Usage: $0 release_name target_name brac_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
brac_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi

if [ ! -f "$brac_PROPS" -o ! -r "$brac_PROPS" ]; then
    echo "Couldn't open brac config file $brac_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi

echo -n "Deploying brac..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $DEPLOY_DIR/conf/brac.conf $brac_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $DEPLOY_DIR/conf/brac.conf $brac_PROPS
chmod g+r $DEPLOY_DIR/conf/brac.conf


echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE



# 1st post-deployment step - create softlink to ojdbc14.jar such that BRAC logins are possible
if mkdir -p $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib
then
  cd $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib
  ln -s /tui/iscape/program/instantclient_10_2_sparc_32/ojdbc14.jar ojdbc14.jar
fi
if [ ! -f $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib/ojdbc14.jar ]
then
  echo "Post-Deploy step failed. Unable to softlink from $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/shared/lib/ojdbc14.jar to /tui/iscape/program/instantclient_10_2_sparc_32/ojdbc14.jar" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE



# 2nd post-deployment step - As appropriate, copy the datacash.conf from cvs
dcname=brac_datacash.conf
# Determine cvs directory
cvs_datacash_filespec=`echo $brac_PROPS | sed "s:[^/]*/[^/]*$:brac/${dcname}:"`
dev_datacash_filespec=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcname
#echo "DEBUG: cvs_datacash_filespec is $cvs_datacash_filespec     dev_datacash_filespec is $dev_datacash_filespec"
if [ -r $cvs_datacash_filespec ]
then
  if [ -r $dev_datacash_filespec ]
  then
    mv $dev_datacash_filespec ${dev_datacash_filespec}_fromdev
    cp -p $cvs_datacash_filespec $dev_datacash_filespec
    echo "  $dcname from cvs for this environment has been deployed."
  else
    cp -p $cvs_datacash_filespec $dev_datacash_filespec
    echo "  INFO: $dcname from CVS has been deployed, but this release seems not to need it."
  fi
else
  if [ -r $dev_datacash_filespec ]
  then
    echo " "
    echo "  *********************************************************************************************"
    echo "  WARNING: This build appears to require a $dcname, yet no such file is provided from CVS"
    echo "  *********************************************************************************************"
    echo " "
  fi
fi
# Support the old, temporary naming of datacash. conf if it exists
dcname=datacash.conf
cvs_datacash_filespec=`echo $brac_PROPS | sed "s:[^/]*/[^/]*$:brac/${dcname}:"`
dev_datacash_filespec=$TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/$dcname
if [ -r $cvs_datacash_filespec ]
then
  if [ -r $dev_datacash_filespec ]
  then
    mv $dev_datacash_filespec ${dev_datacash_filespec}_fromdev 2> /dev/null
    cp -p $cvs_datacash_filespec $dev_datacash_filespec
    echo "  $dcname from cvs for this environment has been deployed."
  else
    cp -p $cvs_datacash_filespec $dev_datacash_filespec
    echo "  INFO: $dcname from CVS has been deployed, but this release seems not to need it."
  fi
else
  if [ -r $dev_datacash_filespec ]
  then
    echo " "
    echo "  *********************************************************************************************"
    echo "  WARNING: This build appears to require a $dcname, yet no such file is provided from CVS"
    echo "  *********************************************************************************************"
    echo " "
  fi
fi

echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE



echo "Done." | tee -a $LOGFILE
