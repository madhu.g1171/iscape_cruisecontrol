#!/bin/sh

############################################################################
#
# editproperties.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# This does the usual substitution, 
# BUT DOES SUPPORT '=' IN THE VALUE BEING SUBSTITUTED
# as requested in nandan's email to Andy Chapman Thu 28/02/2008 11:36
#
# Location: MASTER version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# History:
# 08/04/08 AC Amended such that works with Bourne shell
# 08/04/08 AC Amended such that works with Bourne shell
# 12/04/08 AC Fix bug in v1.3. All since is redundant
# 18/11/08 AC Make the created conf file readable by world
# 16/02/09 AC Remove addition of safe EOF, as check4redundant now does this
# 13/05/10 RB Modified the egrep line print $1 to print $0 to pick all values after = symbol while replacing the env values.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################

if [ "$#" -lt "2" ]
then
	echo "Usage: $0 properties_file substitutes_file"
	echo "Applies changes from substitutes_file to properties_file"
	exit
fi

# Original properties file is specified as first arg
PROPERTIES_FILE=$1
SUBSTITUTES_FILE=$2

# Optional third argument is log file. If not specified
# then output goes to stdout
if [ $# = 3 ]; then
	LOGFILE="$3"
fi

edit_commands="`mktemp`"
# Note: use '#' as the pattern separator in the sed statement because it's unlikely to occur in the 
# properties file. Other characters like ':' and '/' are very likely to occur.
# Strip out all the comments from the new properties file first (they're lines beginning with 
# optional spaces or tabs and a # character).
# Then split the file at the equals sign to give the property name and its new value.
# Then we munge these to produce a sed command that will find the property name followed by
# an equals sign and some text, and insert the new value after the equals sign in place of the old
# All these commands are stored in a temporary file

# prior to this change, used awk -F'=' - which cut off subsequent = in the value
#egrep -v '^[ 	]*#' $SUBSTITUTES_FILE | awk -F'=' '{print $1, $2}' | while read propertyname newvalue; do
#	echo "s#^\\($propertyname\\)=.*\$#\\1=$newvalue#"
#done  > $edit_commands


#egrep -v '^[ 	]*#' $SUBSTITUTES_FILE | awk '{print $1}' | while read aline; do
#	echo "s#^\\(${aline%%=*}\\)=.*\$#\\1=${aline#*=}#"
#done  > $edit_commands
# cannot use ${var%%nn} in sh


egrep -v '^[ 	]*#' $SUBSTITUTES_FILE | awk '{print $0}' | while read aline; do
  thisitemname=`echo $aline | cut -d"=" -f1`
  thisitemvalue=`echo $aline | sed -e "s#^${thisitemname}=##"`
  echo "s#^\\(${thisitemname}\\)=.*\$#\\1=${thisitemvalue}#"
done  > $edit_commands

	
# Now we hand the file of edit commands to sed to make the changes.
# The output is put in a new temporary file and we do a little bit of shuffling to
# make a backup copy and rename the temporary file to replace the original
temp_file="`mktemp`"
sed -f $edit_commands < $PROPERTIES_FILE > $temp_file
mv $PROPERTIES_FILE $PROPERTIES_FILE.preSubstitute
cp $temp_file $PROPERTIES_FILE
chmod a+r $PROPERTIES_FILE
chmod ug+w $PROPERTIES_FILE
# Final Tidy
rm $PROPERTIES_FILE.preSubstitute

