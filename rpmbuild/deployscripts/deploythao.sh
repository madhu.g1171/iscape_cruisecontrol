#!/bin/sh

# 24/3/9 AC: replace existing deploythao.sh with the existing deployaccom.sh, & comment that indicatij git should be redundant
# arising from Yasin email Tue 24/03/2009 09:07


############################################################################
#
# deploythao.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the thomsonao webapp
# Usage: sh deploythao.sh release_name target_name thomson.properties [logfile]
#
# Examples: deploythao.sh is01.01.01 iscapet1 thomson-iscapet1.properties powersearch.xsl
#
# Assumes the thomsonao rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the thomson artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Location: MASTER version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Issues:
# - Does not create a manifest file
#
# History follows
# 28/01/09 A Chapman     Check for unused substitution values (added as missing!)
# 24/03/09 A Chapman     Subst contents of this file with deployaccom.sh (which uses check4redundant_iscape_tags.sh)
#
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################

TUI_ISCAPE=/tui/iscape
. $TUI_ISCAPE/program/setenv.sh

usage="Usage: $0 release_name target_name thomson_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
THOMSON_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$THOMSON_PROPS" -o ! -r "$THOMSON_PROPS" ]; then
    echo "Couldn't open thomson config file $THOMSON_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi

echo -n "Deploying thomson ao..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
THOMSON_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $THOMSON_DEPLOY_DIR/conf/accom.conf $THOMSON_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $THOMSON_DEPLOY_DIR/conf/accom.conf $THOMSON_PROPS
chmod g+r $THOMSON_DEPLOY_DIR/conf/accom.conf

#echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

echo "Done." | tee -a $LOGFILE
