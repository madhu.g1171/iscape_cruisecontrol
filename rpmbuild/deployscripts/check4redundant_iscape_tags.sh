#!/bin/sh

############################################################################
#
# check4redundant_iscape_tags.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Checks for (unused) items in iScape substitutes_file not in properties_file
#  (ie for thomson, brac or flight installations)
#
# Location: master version of this file is held in: {see HeadURL below}
#  though it will need manually updating into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for wish deployments, and likely for all other brands too. 
#
# History follows
# 30/07/07 A Chapman     Added to CVS
# 09/04/08 A Chapman     Prevent FCSUN_TEMPLATE_VERSION appearing in unused substitution values
# 08/08/08 A Chapman     Changed to remove *_TEMPLATE_VERSION from warning for wish, fcfalcon, thao, fcao
# 30/09/08 A Chapman     Extend for cps & tracsservice. Add SVN keywords, & replace cvs Source with svn HeadURL
# 14/11/08 A Chapman     Support genericcpsclient
# 26/11/08 A Chapman     When check for redundant/multiple items, dont count '^item', instead count '^item='
#                         To avoid counting superstrings (as in CPS)
# 28/11/08 A Chapman     Exclude GroundTrader items (they are done by a different process)
# 15/12/08 A Chapman     Avoid the old 'last-line provided by Devt lacks trailing CR' problem - for this redundant test only
# 19/01/09 A Chapman     Support groundtrader
# 12/02/09 A Chapman     Support psdataadmin (PowerSearch Admin Tool)
# 16/02/09 A Chapman     Update such that THIS script (which is invoked by all clients) creates the .fromdev variant, & ensures a proper CRLF at EOF
# 16/02/09 A Chapman     Perform dos2unix on the file provided by development
# 13/07/09 Ramesh Babu   Support wss aka managemybooking
# 17/09/09 Ramesh Babu   Support eBorders brand
# 06/10/09 Ramesh Babu   Support upload brand
# 19/10/09 A Chapman     Bugfix. omit 'spaces or tabs' had become 'omit spaces' (Tab char lost in grep -v '^[  ). 
#                          Affected wss conf file (not fixed, to test this)
# 07/12/09 A Chapman     Support pluto brand
# 18/12/09 A Chapman     Support Experimental sprocketRPM brand
# 06/04/10 A Chapman     Formalise support for first (of 6) Sprocket brands = sprocketCTH : Currently explicitly named. Plan for Generic Sprocket solution later.
# 16/04/10 P Prajwala    Support acommap brand
# 17/05/10 Ramesh Babu   Extended support Uncovered Web & Admin applications.
# 25/05/10 Ramesh Babu   Updated the uncovered web file name from uncoveredWeb to uncoveredweb.
# 02/06/10 Ramesh Babu   Updated the Uncoveredad Admin file name from uncoveredAdmin to uncoveredadmin.
# 13/07/10 Prajwala P	 Updated for VOL
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################



# METHODS
# list_redundant_tags - 1st param is the properties file from development. 2nd param is the substitutes file-- method definition
list_redundant_tags()
{
  egrep -v '^[ 	]*#' $2 | egrep -v '^[ 	]*$' | egrep -v "^$3" | egrep -v "^CPS.GROUNDTRADER." \
      | awk -F'=' '{print $1}' | while read propertyname; do
        nummatch=`grep "^${propertyname}=" $1 | wc -l`
	if [ $nummatch != 1 ]
	then
		echo "$propertyname"
	fi
done
}


display_warning()
{
  echo " "
  echo " "
  echo " ************************************************************************"
  echo " *                                                                      *"
  echo " * WARNING                                                              *"
  echo " * =======                                                              *"
  echo " *                                                                      *"
  echo " * $* "
  echo " *                                                                      *"
  echo " ************************************************************************"
  echo " "
}

apply_dos2unix()
{
  # removes any ^M line endings from the specified file. 
  # only parameter specifies file to clean. Can be absolute/relative/no path.
  # Beware: dos2unix behaves differently on solaris & unix. Failsafe.
  echo " Performing dos2unix on file: $1"
  ostype=`uname`
  if [ "$ostype" = "SunOS" ]
  then
    mv $1 ${1}_preTounix
    dos2unix ${1}_preTounix > $1 2>/dev/null
    rm ${1}_preTounix
  else
    dos2unix $1 > /dev/null 2>/dev/null
  fi
}





# Main body of script

if [ "$#" -lt "2" ]
then
	echo "Usage: $0 properties_file substitutes_file"
	echo "Checks for (unused) items in substitutes_file not in properties_file"
	exit
fi

# Original properties file is specified as first arg
PROPERTIES_FILE=$1
SUBSTITUTES_FILE=$2



# Determine the name of the template id line to ignore
line_to_ignore="noexist"
if [ `echo $2 | egrep "thomson-iscape.properties|wish-iscape.properties" | wc -l` = 1 ]
then
  # This also applies to WISH
  line_to_ignore="THOMSON_TEMPLATE_VERSION|^WISH_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "flight-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="FLIGHT_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "brac-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="BRAC_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "fcsun-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="FCSUN_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "fcfalcon-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="FCFALCON_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "thao-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="THAO_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "fcao-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="FCAO_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "commonpaymentserver-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="CPS_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "tracsserviceportland-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="TRACSSERVICEPORTLAND_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "tracsservice-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="TRACSSERVICE_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "genericcpsclient-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="GENERICCPSCLIENT_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "groundtrader-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="GROUNDTRADER_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "visiongateway-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="VISIONGATEWAY_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "psdataadmin-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="PSDATAADMIN_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "webcruise-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="WEBCRUISE_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "wss-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="WSS_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "eBorders-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="EBORDERS_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "upload-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="UPLOAD_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "pluto-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="PLUTO_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "sprocketCTH-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="SPROCKETCTH_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "laterates-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="LATERATES_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "accommap-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="ACCOMMAP_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "uncoveredweb-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="UNCOVEREDWEB_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "uncoveredadmin-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="UNCOVEREDADMIN_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "bookingenquiry-iscape.properties" | wc -l` = 1 ]
then
  line_to_ignore="BOOKINGENQUIRY_TEMPLATE_VERSION"
fi
if [ `echo $2 | grep "visionoscardataloader-iscape.properties"  | wc -l` = 1 ]
then
  line_to_ignore="VISIONOSCARDATALOADER_TEMPLATE_VERSION"
fi


# Avoid the old 'last-line provided by Devt lacks trailing CR' problem - for this redundant test only
# Remember: this is a check & warning. The actual fix is implemented by editproperties.sh (Horrible duplication)
file_from_devt=${PROPERTIES_FILE}.fromdev
cp -p $PROPERTIES_FILE $file_from_devt
echo " " >> $PROPERTIES_FILE
echo "# Final line - added by check4redundant_iscape_tags.sh " >> $PROPERTIES_FILE
apply_dos2unix $PROPERTIES_FILE



# Perform the check
if [ `list_redundant_tags $PROPERTIES_FILE $SUBSTITUTES_FILE $line_to_ignore | wc -l` -ne 0 ] 
then
  list_redundant_tags $PROPERTIES_FILE $SUBSTITUTES_FILE $line_to_ignore 
	echo "PP -> $PROPERTIES_FILE -> $SUBSTITUTES_FILE -> $line_to_ignore"
  display_warning "Following items in $SUBSTITUTES_FILE are unused by (or occur more than once in) $PROPERTIES_FILE" 
  list_redundant_tags $PROPERTIES_FILE $SUBSTITUTES_FILE $line_to_ignore
  echo " "
  echo " "
fi
#
#rm $file_from_devt

