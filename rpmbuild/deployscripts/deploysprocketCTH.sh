#!/bin/sh

# NEEDS BIT TIDY - once proven OK

# Then see if we can create a GENERIC version for all sprockets (or all everything, come to that...)

# Mix of 2 different case. Trying not to make it 3 !!


# This file is created for developing a Sprocket RPM build/deploy solution.
# It may never be used for real
# Andy Chapman 17/12/09
#
#
# Currently, it is the default deploysprocketplus.sh, with just this comment added at the top
#



############################################################################
#
# deploysprocketCTH.sh 		- for Sprocket (Thomson) CRUISE
#   as built-in by the java javarpmbuild process
#
# Post-install script to deploy the sprocketplus webapp
# Usage: sh deploysprocketplus.sh release_name target_name sprocketplus.properties [logfile]
#
# Examples: deploysprocketplus.sh is01.01.01 iscapet1 sprocketplus-iscapet1.properties powersearch.xsl
#
# Assumes the sprocketplus rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the sprocketplus artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This EXPERIMENTAL script is used for sprocketPlus deployments, 
#
# History follows
# 01/04/10 01 A Chapman     Created Branded version of this file.
#
#  ***  Inherited comment: Suggest deploy script checks the jar has been installed  *** (spring jar???)
#
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################


DEPLOYSPROCKETCTH_SH_TEMPLATE=1
deploysprocketCTH_sh_cvsrev="$Revision$"
DEPLOYSPROCKETCTH_SH_REVISION=`echo $deploysprocketCTH_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name sprocketCTH_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
SPROCKETCTH_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$SPROCKETCTH_PROPS" -o ! -r "$SPROCKETCTH_PROPS" ]; then
    echo "Couldn't open sprocketCTH config file $SPROCKETCTH_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/$LOCAL_ISCAPE_ENVIRONMENT"




echo -n "Deploying sprocketCTH..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
SPROCKETCTH_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $SPROCKETCTH_DEPLOY_DIR/conf/sprocketCTH.conf $SPROCKETCTH_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $SPROCKETCTH_DEPLOY_DIR/conf/sprocketCTH.conf $SPROCKETCTH_PROPS
chmod g+r $SPROCKETCTH_DEPLOY_DIR/conf/sprocketCTH.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYSPROCKETCTH_SH_TEMPLATE=$DEPLOYSPROCKETCTH_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYSPROCKETCTH_SH_CVSREVISION=$DEPLOYSPROCKETCTH_SH_REVISION" >> $manifestfile
echo "SPROCKETCTH-ISCAPE_PROPERTIES_TAGGED_SPROCKETCTH_TEMPLATE="`cat $SPROCKETCTH_PROPS | egrep "^SPROCKETCTH_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# Nth post-deployment step - describe purpose here

# 1st post-deployment step - early sprocket deployscript : As of 16/3/10 - no action required

apply_dos2unix()
{
  # removes any ^M line endings from the specified file. 
  # only parameter specifies file to clean. Can be absolute/relative/no path.
  # Beware: dos2unix behaves differently on solaris & unix. Failsafe.
  echo " Performing dos2unix on file: $1"
  ostype=`uname`
  if [ "$ostype" = "SunOS" ]
  then
    mv $1 ${1}_preTounix
    dos2unix ${1}_preTounix > $1 2>/dev/null
    rm ${1}_preTounix
  else
    dos2unix $1 > /dev/null 2>/dev/null
  fi
}

echo "Early sprocket deployscript : As of 16/3/10 - no action required"


#apply_dos2unix ../rpmiscape/soc-environment.properties

#echo ""
#echo "This RPM provides: rpmiscape/soc-environment.properties (& dos2unix)"
#echo "But does NOT overwrite the existing /tui/sprocket/conf/soc-environment.properties "
#echo ""
#echo "info:"
#ls -al ../rpmiscape/soc-environment.properties /tui/sprocket/conf/soc-environment.properties 
#echo ""
#echo ""
#echo "Now DIFF:"
#diff ../rpmiscape/soc-environment.properties /tui/sprocket/conf/soc-environment.properties
#echo ""
#echo ""
#echo ""


echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE




echo "Done." | tee -a $LOGFILE
