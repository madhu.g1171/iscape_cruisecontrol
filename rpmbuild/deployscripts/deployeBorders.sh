#!/bin/sh

############################################################################
#
# deployeBorders.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the eBorders webapp
# Usage: sh deployeBorders.sh release_name target_name eBorders.properties [logfile]
#
# Examples: deployeBorders.sh eBorders01.00.00 iscapet1 eBorders-iscapet1.properties 
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for eBorders deployments. 
#
# History follows
# 17/07/09 Ramesh Babu   Created the file to deploy eBorders application.
# 18/07/09 A Chapman     Add check that the EL jars are installed in the tomcat binary structure
# 01/10/09 Ramesh Babu   Updated routingfilepath parameter to pick iscape_routing file as per TS and CPS deployments.
# 05/10/09 Ramesh Babu   After discussion with Jason, Commented i3 support installation (post deployment 1) script. Fri 02/10/2009 11:54 Andy mail.
# 12/10/09 Ramesh Babu   Modified script to get eborders.conf file as per second release and standard process.
# 02/11/10 Ramesh Babu   Removed PAT servers list from Precise servers list section as per discussion with Chris on 2/11/2010.
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
#
############################################################################


DEPLOYEBORDERS_SH_TEMPLATE=1
deployeBorders_sh_cvsrev="$Revision$"
DEPLOYEBORDERS_SH_REVISION=`echo $deployeBorders_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name eBorders _config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
EBORDERS_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$EBORDERS_PROPS" -o ! -r "$EBORDERS_PROPS" ]; then
    echo "Couldn't open eBorders config file $EBORDERS_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"


echo -n "Deploying eBorders ..."  | tee -a $LOGFILE

# For tomcat5*, check that the EL jar files have been added to the tomcat install directories
if [ "$TOMCAT55" != "" ]
then
  if [ ! -f $TOMCAT55/common/lib/el-api-*.jar -o ! -f $TOMCAT55/common/lib/el-impl-*.jar ]
  then
    echo ""
    echo "ERROR: MISSING JARs"
    echo "       For Tomcat5*, eBorders requires el-api-*.jar & el-impl-*.jar to exist in :"
    echo "         $TOMCAT55/common/lib"
    echo "These files are missing"
    echo "They can be procured from the build machine, from ~bob/ivy-repo"
    echo "with checksums (on solaris) using 'sum el-*.jar':"
    echo "   14057 58 ivy-repo/el-api-1.0.jar"
    echo "   64130 141 ivy-repo/el-impl-1.0.jar"
    echo ""
    echo "Now ABORTING. Please re-run this deployscript once the jars are present."
    echo ""
    echo ""
    exit 1
  fi
fi

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
EBORDERS_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $EBORDERS_DEPLOY_DIR/conf/eborders.conf $EBORDERS_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $EBORDERS_DEPLOY_DIR/conf/eborders.conf $EBORDERS_PROPS
chmod g+r $EBORDERS_DEPLOY_DIR/conf/eborders.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYEBORDERS_SH_TEMPLATE=$DEPLOYEBORDERS_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYEBORDERS_SH_CVSREVISION=$DEPLOYEBORDERS_SH_REVISION" >> $manifestfile
echo "EBORDERS-ISCAPE_PROPERTIES_TAGGED_EBORDERS_TEMPLATE="`cat $EBORDERS_PROPS | grep "^EBORDERS_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp



echo "Deployment performed. No Post-Deployment Step(s)" | tee -a $LOGFILE

echo "Done." | tee -a $LOGFILE
