#!/bin/sh

############################################################################
#
# deploybookingenquiry.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the aonbookingenquiry webapp
# Usage: sh deploybookingenquiry.sh release_name target_name aonbookingenquiry.properties [logfile]
#
# Examples: deploybookingenquiry.sh 01.01.01 iscapet1 aonbookingenquiry-iscapet1.properties 
#
# Assumes the bookingenquiry rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the bookingenquiry artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for bookingenquiry deployments and is amended based on Pluto
#
# Notes:
# - i3/precise not yet added. Add hostnames
#
#
# History follows
# 22/04/10 P Prajwala Created for new brand aonbookingenquiry
# 02/11/10 Ramesh Babu   Removed PAT servers list from Precise servers list section as per discussion with Chris on 2/11/2010.

# @(#) last changed by: $Author$ $Revision$ $Date$
# # $HeadURL$
#
############################################################################

DEPLOYAONBOOKINGENQUIRY_SH_TEMPLATE=1
deployaonbookingenquiry_sh_svnrev="$Revision$"
DEPLOYAONBOOKINGENQUIRY_SH_REVISION=`echo $deployaonbookingenquiry_sh_svnrev | cut -d" " -f2`


declare_error()
{
  echo " "
  echo "***********************************************************************"
  echo $1
  echo "***********************************************************************"
  echo " "
}



# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`



usage="Usage: $0 release_name target_name aonbookingenquiry_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
AONBOOKINGENQUIRY_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$AONBOOKINGENQUIRY_PROPS" -o ! -r "$AONBOOKINGENQUIRY_PROPS" ]; then
    echo "Couldn't open aonbookingenquiry config file $AONBOOKINGENQUIRY_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"



echo -n "Deploying aonbookingenquiry..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
AONBOOKINGENQUIRY_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $AONBOOKINGENQUIRY_DEPLOY_DIR/conf/bookingenquiry.conf $AONBOOKINGENQUIRY_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $AONBOOKINGENQUIRY_DEPLOY_DIR/conf/bookingenquiry.conf $AONBOOKINGENQUIRY_PROPS
chmod g+r $AONBOOKINGENQUIRY_DEPLOY_DIR/conf/bookingenquiry.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYAONBOOKINGENQUIRY_SH_TEMPLATE=$DEPLOYAONBOOKINGENQUIRY_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYAONBOOKINGENQUIRY_SH_CVSREVISION=$DEPLOYAONBOOKINGENQUIRY_SH_REVISION" >> $manifestfile
echo "AONBOOKINGENQUIRY-ISCAPE_PROPERTIES_TAGGED_AONBOOKINGENQUIRY_TEMPLATE="`cat $AONBOOKINGENQUIRY_PROPS | grep "^AONBOOKINGENQUIRY_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp

echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE



# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "ukpwap07" \
  -a "$thishost" != "ukpwap06" \
  -a "$thishost" != "ukpwap30" \
  -a "$thishost" != "ukpwap31" \
  -a "$thishost" != "ukpwap01" \
  -a "$thishost" != "ukpwap02" ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar \
  -a ! -r /tui/iscape/program/tomcat5.0/jakarta-tomcat-5.0.28/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED" | tee -a $LOGFILE
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


# 2nd post-deployment step - As appropriate, copy the StarLicense.properties from cvs
# Determine cvs directory
brfname="StarLicense.properties"
cvs_brfile="$TUI_ISCAPE/config/$config_module/$LOCAL_ISCAPE_ENVIRONMENT/bookingenquiry/$brfname"
if [ -r $cvs_brfile ]
then
    cp -p $cvs_brfile $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/
    echo "  $brfname from cvs for this environment has been deployed."
else
    echo " "
    echo "  *********************************************************************************************"
    echo "  ERROR: This build appears to require a $brfname, yet no such file is provided from CVS"
    echo "  *********************************************************************************************"
    echo " "
fi

echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
