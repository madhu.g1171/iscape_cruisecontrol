#!/bin/sh

############################################################################
#
# deployuncoveredAdmin.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the uncoveredAdmin webapp
# Usage: sh deployuncoveredAdmin.sh release_name target_name uncoveredAdmin.properties [logfile]
#
# Examples: deployuncoveredAdmin.sh uncoveredAdmin01.00.00 iscapet1 uncoveredAdmin-iscapet1.properties 
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
# This script is used for uncoveredAdmin deployments. 
#
# History follows
# 01/06/10 Ramesh Babu   Created the file to test Holidays Uncovered application.
#
#
# @(#) last changed by: $Author: xxxx $ $Revision: xxxx $ $Date: xxxx $
# $HeadURL: xxxxx $
#
#
############################################################################


DEPLOYUNCOVEREDADMIN_SH_TEMPLATE=1
deployuncoveredAdmin_sh_cvsrev="$Revision: xxxx $"
DEPLOYUNCOVEREDADMIN_SH_REVISION=`echo $deployuncoveredAdmin_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name uncoveredAdmin _config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
UNCOVEREDADMIN_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$UNCOVEREDADMIN_PROPS" -o ! -r "$UNCOVEREDADMIN_PROPS" ]; then
    echo "Couldn't open UncoveredAdmin config file $UNCOVEREDADMIN_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"


echo -n "Deploying UncoveredAdmin ..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
UNCOVEREDADMIN_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $UNCOVEREDADMIN_DEPLOY_DIR/conf/uncoveredAdmin.conf $UNCOVEREDADMIN_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $UNCOVEREDADMIN_DEPLOY_DIR/conf/uncoveredAdmin.conf $UNCOVEREDADMIN_PROPS
chmod g+r $UNCOVEREDADMIN_DEPLOY_DIR/conf/uncoveredAdmin.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYUNCOVEREDADMIN_SH_TEMPLATE=$DEPLOYUNCOVEREDADMIN_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYUNCOVEREDADMIN_SH_CVSREVISION=$DEPLOYUNCOVEREDADMIN_SH_REVISION" >> $manifestfile
echo "UNCOVEREDADMIN-ISCAPE_PROPERTIES_TAGGED_UNCOVEREDADMIN_TEMPLATE="`cat $UNCOVEREDADMIN_PROPS | grep "^UNCOVEREDADMIN_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp



echo "Deployment performed. No Post-Deployment Step(s)" | tee -a $LOGFILE

echo "Done." | tee -a $LOGFILE
