#!/bin/sh

############################################################################
#
# deploytracsservice.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the iscape deploytracsservice webapp
#
# Usage: MAY NEED UPDATING  sh deploydeploytracsservice.sh release_name target_name deploytracsservice.properties [logfile]
# Example: deploydeploytracsservice.sh ts01.01.01 iscapet1 deploytracsservice-iscapet1.properties powersearch.xsl
#
# Assumes the deploytracsservice rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the deploytracsservice artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it SHOULD automatically overwrite into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#   {but does not from the /tmp/donotdelete build {AC 9/1/09}}
#
# Notes:
# - i3/precise not yet added. Add hostnames
#
#
# History follows
# 09/10/08 R Hands     created as copy from thomson script
# 15/10/08 A Chapman   Adjust as required for tracsservice-portland
# Dec08    Prajwala P  Adapted for 'generic' TracsService (i.e. Phase2 &later) from deploytracsserviceportland.sh
# 09/02/09 A Chapman   (Temporarily, until project provides FixCRLF solution) do dos2unix on the conf file
# 16/02/09 A Chapman   Remove dos2unix, as now done by check4redundant_iscape_tags.sh
# 23/09/09 A Chapman   Generate the Distributed Cache config items
# 15/10/09 A Chapman   Fix such that it works when invoked by ssh from uk?dbo01 (Andrew Roberts emails Thu 15/10/2009 10:01 & 10:08)
# 14/01/10 A Chapman   For distributed caching: Reorder hosts such that local instances come first. (arising from Andrew Roberts email Wed 13/01/2010 14:11)
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################

DEPLOYTRACSSERVICE_SH_TEMPLATE=1
deploytracsservice_sh_cvsrev="$Revision$"
DEPLOYTRACSSERVICE_SH_REVISION=`echo $deploytracsservice_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`
thishostenv=`echo $thishost | cut -b1-3`


usage="Usage: $0 release_name target_name deploytracsservice_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
TSP_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$TSP_PROPS" -o ! -r "$TSP_PROPS" ]; then
    echo "Couldn't open deploytracsservice config file $TSP_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/${cvs_misc_subdir}$LOCAL_ISCAPE_ENVIRONMENT"




echo -n "Deploying deploytracsservice..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
TSP_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $TSP_DEPLOY_DIR/conf/tracsservice.conf $TSP_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $TSP_DEPLOY_DIR/conf/tracsservice.conf $TSP_PROPS
chmod g+r $TSP_DEPLOY_DIR/conf/tracsservice.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYTRACSSERVICE_SH_TEMPLATE=$DEPLOYTRACSSERVICE_SH_TEMPLATE" >> $manifestfile
echo "deploytracsservice_sh_cvsrevISION=$DEPLOYTRACSSERVICE_SH_REVISION" >> $manifestfile
echo "TSP-ISCAPE_PROPERTIES_TAGGED_TSP_TEMPLATE="`cat $TSP_PROPS | egrep "^TRACSSERVICE_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$envtype" = "not_pat_nor_prod" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# 1st post-deployment step - Conditionally Insert i3-precise listener into server.xml
inject_i3_for_this_instance=true
if [ "$thishost" != "uktNOMATCH_A" \
  -a "$thishost" != "ukpNOMATCH_B" \
  -a "$thishost" != "ukpNOMATCH_C" ]
then
  inject_i3_for_this_instance=false
fi
this_instance=`echo $TARGET_NAME | sed -e "s/^.*\(.\)$/\1/"`
# Do NOT verify a valid instance here. Buildscripts is not the right place
if [ "$this_instance" != "a" \
  -a "$this_instance" != "b" ]
then
  inject_i3_for_this_instance=false
fi
if [ ! -r /tui/iscape/program/tomcat5.5/apache-tomcat-5.5.12/server/lib/indepthmetric.jar \
  -a ! -r /tui/iscape/program/tomcat5.0/jakarta-tomcat-5.0.28/server/lib/indepthmetric.jar ]
then
  inject_i3_for_this_instance=false
fi

if [ "$inject_i3_for_this_instance" = "true" ]
then 
  echo "i3 JMX Metrics monitoring is being ENABLED" | tee -a $LOGFILE
  if [ ! -w $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml ]
  then
    echo "Post-Deploy step 1 failed. Unable to write to $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml" | tee -a $LOGFILE
    exit 1
  else
    mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml \
      $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org
    cat $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml_org | \
        sed -e \
          "s~\(<\!-- Start of INSERT I3 PRECISE LISTENER1 HERE -->\)~\1<Listener className=\"com.precise.javaperf.extensions.tomcat.JMXMetricsLoaderListener\"/>~" \
        > $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/conf/server.xml
  fi
else
  echo "i3 JMX Metrics monitoring is not required for this instance" | tee -a $LOGFILE
fi
echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


# 2nd post-deployment step - Move wars from artifacts into webapps
mv $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/artifacts/tracsservice.war $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/webapps
echo "Post-Deployment Step 2 complete." | tee -a $LOGFILE


# 3rd post-deployment step - Deploy the xslt files
mkdir $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/xsl_files
cd $TUI_ISCAPE/releases/$RELEASE_NAME/$TARGET_NAME/publish/xsl_files
tar -xf $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/tsp2_xslt_files.tar
echo "Post-Deployment Step 3 complete." | tee -a $LOGFILE


# 4th post-deployment step - Generate the Distributed Caching .rmiUrls config items
distributed_caching_seed_port_for_tracsservice=49225

# subroutines used

debug()
{
  :
#  echo "DEBUG: $*"
}

fatal()
{
  echo ""
  echo "FATAL:  $1"
  test "$2" = "" || echo "  $2"
  test "$3" = "" || echo "  $3"
  test "$4" = "" || echo "  $4"
  test "$5" = "" || echo "  $5"
  echo "ABORTING"
  echo ""
  exit 1
}

get_my_ip_address()
{
  if [ "`hostname | cut -b1-2`" = "uk" ]
  then
    #echo "This IS an infotec ZONE"
    # Add below 1-line check, just incase /usr/sbin/ifconfig not exist on this zone
    test -f /usr/sbin/ifconfig || echo "WARNING: own IP address not detected : TS rmiUrls will need amending :  ATTENTION"
    my_ip=`/usr/sbin/ifconfig -a | grep "inet " | tail -1 | cut -d" " -f2`
  else
    if [ `cat /proc/version | grep -i "ubuntu" > /dev/null; echo "$?"` = "0" ]
    then
      #echo "its ubuntu"
      my_ip=`ifconfig -a | grep "inet " | head -1 | cut -d":" -f2 | cut -d" " -f1`
    else
      if [ `cat /proc/version | grep -i "SUSE" > /dev/null; echo "$?"` = "0" ]
      then
        #echo "its SUSE"
        my_ip=`/sbin/ifconfig -a | grep "inet " | head -1 | cut -d":" -f2 | cut -d" " -f1`
      else
        echo "CANNOT IDENTIFY O/S: hence cannot ID host IP"
        exit 1
      fi
    fi
  fi
}

get_dcport_given_instance()
{
  instance_to_match=$1
  listenport=`expr $distributed_caching_seed_port_for_tracsservice - 1`
  alphabet="a b c d e f g h i j k l m n o p q r s t u v w x y z 9"
  for this_alpha in $alphabet
  do
    listenport=`expr $listenport + 1`
    if [ "$this_alpha" = "$instance_to_match" ]
    then
      break
    fi
    test "$this_alpha" = "9" && fatal "Passed invalid instance = $this_instance"
  done
}

trim()
{
  echo $1
}


# Create the distributed cache config items: cacheManagerPeerProviderFactory.rmiUrls
# This must list every tracsservice instance, EXCEPT for this instance now being deployed.
groupsfile=$routingfilepath/tracsservice/groups
hostsfile=$routingfilepath/tracsservice/hosts
get_my_ip_address # set $my_ip
#echo "Determined that My IP address is '$my_ip'"

if [ ! -r $groupsfile -o ! -r $hostsfile ]
then
  fatal "Missing file(s). Either hosts or groups is missing from $routingfilepath/tracsservice" \
    "Hence cannot create the distributed caching configuration data."
fi

instances_in_this_group=`grep -v "^#" $groupsfile | cut -d":" -f2 | grep $this_instance | sed -e "s/,/ /g"`
test "$instances_in_this_group" = "" && fatal "Current instance ($this_instance) does not exist in $groupsfile"
debug  "instances_in_this_group are $instances_in_this_group"

# check is just 1 active line in hosts file
countofactivelines=`grep -v "^#" $hostsfile | grep -v "^$" | wc -l`
if [ "`trim $countofactivelines`" != "1" ]
then
  fatal "In TracsService hosts file '$1' there must be just one non-comment line. There are $countofactivelines"
fi
csv_hosts=`grep -v "^#" $hostsfile | grep -v "^$"`
test "$csv_hosts" = "" && fatal "List of DistributedCaching hosts in $hostsfile is empty."

# check my_ip is included in the list of hosts
# warning, test (in bourne shell) throws error if hosts line is space separated, rather than comma separated.
test `echo "$csv_hosts" | grep "$my_ip"` || fatal "Local IP ($my_ip) not found in hosts list ($csv_hosts)"

# put my_ip at the front of list, and convert to space separated
ts_space_separated=`echo $csv_hosts | sed -e "s/,/ /g"`
ts_hosts="$my_ip"
for ip in $ts_space_separated
do
  if [ "$ip" != "$my_ip" ]
  then
    ts_hosts="$ts_hosts "$ip
  fi
done
debug  "ts_hosts are $ts_hosts"

#get_dcport_given_instance $this_instance
#debug "for this instance=$this_instance, the DistCache listen port is $listenport"

# Create list of all combinations of hosts and ports, EXCLUDING this instance being deployed
listnerlist=""
for a_host in $ts_hosts
do
  for an_instance in $instances_in_this_group
  do
    #debug "This combination is $a_host  $an_instance"
    if [ ! "$a_host" = "$my_ip" -o ! "$an_instance" = "$this_instance" ]
    then
      test "$listnerlist" = "" || listnerlist=${listnerlist}"|//"
      get_dcport_given_instance $an_instance
      listnerlist=${listnerlist}${a_host}:${listenport}/commandCache
    fi
  done
done
listnerlist="{rmiUrls=//"${listnerlist}"}"

debug "Created the below STRING"
debug $listnerlist

get_dcport_given_instance $this_instance # to set listenport
cp -p $TSP_DEPLOY_DIR/conf/tracsservice.conf $TSP_DEPLOY_DIR/conf/tracsservice.conf_tmp_preDC # preserve permissions
cat $TSP_DEPLOY_DIR/conf/tracsservice.conf_tmp_preDC \
 | sed -e "s!^\(tracsservicewebbook.cacheManagerPeerListenerFactory.rmiUrls=\).*!\1{hostName=$my_ip,port=$listenport}!" \
 | sed -e "s!^\(tracsservicewebbook.cacheManagerPeerProviderFactory.rmiUrls=\).*!\1$listnerlist!" \
 > $TSP_DEPLOY_DIR/conf/tracsservice.conf
rm $TSP_DEPLOY_DIR/conf/tracsservice.conf_tmp_preDC

echo "Post-Deployment Step 4 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
