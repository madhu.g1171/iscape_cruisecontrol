#!/bin/sh
#
#   as used by rpmbuild (i.e. NOT javarpmbuild)

echo " "
echo " BELIEVE This script (deploy_cps_groundtrader.sh) has been obsoleted by deploycommonpaymentserver.sh & deploygroundtrader.sh"
echo " "
echo " Please do not use this script (Its history is in svn should you need it)"
echo " and update any instructions that tell you to use this script"
echo " "
echo " Andy Chapman 16-Feb-2009"
echo " "

