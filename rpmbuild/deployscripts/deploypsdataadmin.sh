#!/bin/sh

############################################################################
#
# deploypsdataadmin.sh
#   as used by rpmbuild (i.e. NOT javarpmbuild)
#
# Post-install script to deploy the psdataadmin webapp
# Usage: sh deploypsdataadmin.sh release_name target_name psdataadmin.properties [logfile]
#
# Examples: deploypsdataadmin.sh is01.01.01 iscapet1 psdataadmin-iscapet1.properties 
#
# Assumes the psdataadmin rpm has already been rpm-installed 
# into /tui/iscape/releases/release_name.
# Deploys the psdataadmin artifacts into the deployment directory 
# /tui/iscape/releases/release_name/target_name
# Configures the webapp.
#
# Stopping and starting servers will be handled separately,
# so this script assumes that they are stopped.
#
# Location: master version of this file is held in: {see HeadURL below}
#  upon build, it automatically overwrites into webdevbuildmachine::/usr/src/packages/BUILD/rpmbuild/deployscripts
#
#
# History follows
# 12/02/09 R Hands       Created by copying deploythomson.sh verbatim
# 12/02/09 A Chapman     Amend as required
# 16/02/09 A Chapman     Remove dos2unix, as now done by check4redundant_iscape_tags.sh
# 24/11/09 A Chapman     Remove message about view/source info - as development have now provided this.
#
#
#
# @(#) last changed by: $Author$ $Revision$ $Date$
# $HeadURL$
#
############################################################################


DEPLOYPSDATAADMIN_SH_TEMPLATE=1
deploypsdataadmin_sh_cvsrev="$Revision$"
DEPLOYPSDATAADMIN_SH_REVISION=`echo $deploypsdataadmin_sh_cvsrev | cut -d" " -f2`


# Identify Environment & initialise environment variables
TUI_ISCAPE=/tui/iscape
thishost=`hostname`


usage="Usage: $0 release_name target_name psdataadmin_config_path [logfile_path]"

RELEASE_NAME=$1
TARGET_NAME=$2
PSDATAADMIN_PROPS=$3

# check arguments
if [ "$RELEASE_NAME" = "" -o "$TARGET_NAME" = "" ]
then
    echo $usage 
    exit 1
fi
if [ ! -f "$PSDATAADMIN_PROPS" -o ! -r "$PSDATAADMIN_PROPS" ]; then
    echo "Couldn't open psdataadmin config file $PSDATAADMIN_PROPS for reading"
    exit 2
fi

# Optional fourth argument is log file. If not specified
# then output goes to stdout
if [ $# = 4 ]; then
    LOGFILE="$4"
fi


. $TUI_ISCAPE/program/setenv.sh
. /isutils/set_local_env.sh
routingfilepath="$TUI_ISCAPE/config/$config_module/$LOCAL_ISCAPE_ENVIRONMENT"




echo -n "Deploying psdataadmin..."  | tee -a $LOGFILE

cd $TUI_ISCAPE/releases/$RELEASE_NAME/rpmiscape

tar cf ../publish.tar publish

cd ..

# Create the template CATALINA_BASE directory from the release template
if [ ! -d $TARGET_NAME ]; then
    mkdir -p $TARGET_NAME
fi
cd $TARGET_NAME 
tar xvf ../publish.tar  | tee -a $LOGFILE >/dev/null
rm ../publish.tar

# The location into which the webapp will be deployed
PSDATAADMIN_DEPLOY_DIR="`pwd`/publish"

echo "Checking for unused substitution values"
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/check4redundant_iscape_tags.sh $PSDATAADMIN_DEPLOY_DIR/conf/powersearchadmin.conf $PSDATAADMIN_PROPS

# Run script to change the configuration elements
echo -n "Updating configuration..." | tee -a $LOGFILE
sh $TUI_ISCAPE/releases/$RELEASE_NAME/deployscripts/editproperties.sh $PSDATAADMIN_DEPLOY_DIR/conf/powersearchadmin.conf $PSDATAADMIN_PROPS
chmod g+r $PSDATAADMIN_DEPLOY_DIR/conf/powersearchadmin.conf

# Populate the manifest file, as appropriate
manifestfile=$TUI_ISCAPE/releases/$RELEASE_NAME/manifest.mf
echo "DEPLOYPSDATAADMIN_SH_TEMPLATE=$DEPLOYPSDATAADMIN_SH_TEMPLATE" >> $manifestfile
echo "DEPLOYPSDATAADMIN_SH_CVSREVISION=$DEPLOYPSDATAADMIN_SH_REVISION" >> $manifestfile
echo "PSDATAADMIN-ISCAPE_PROPERTIES_TAGGED_PSDATAADMIN_TEMPLATE="`cat $PSDATAADMIN_PROPS | egrep "^PSDATAADMIN_TEMPLATE_VERSION=" | cut -d"=" -f2 ` >> $manifestfile
echo "RELEASE=$RELEASE_NAME" >> $manifestfile
echo "SETENV_SH_TEMPLATE_VERSION="`echo $SETENV_TEMPLATE_VERSION` >> $manifestfile
echo "SETENV_SH_CVSREVISIONTAG="`echo $SETENV_SH_CVSREVISIONTAG` >> $manifestfile
if [ "$LOCAL_ISCAPE_ENVIRONMENT" = "UNKNOWN" ]
then
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=not_available_for_NonPatProd_hosts" >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=not_available_for_NonPatProd_hosts" >> $manifestfile
else
 echo "ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION="`grep "^ISCAPE_ROUTING_CONFIG_TEMPLATE_VERSION=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 ` >> $manifestfile
 echo "ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG="`grep "^ISCAPE_ROUTING_CONFIG_CVSREVISIONTAG=" $routingfilepath/iscape_routing.config \
      | cut -d"=" -f2 | cut -d" " -f2 ` >> $manifestfile
fi
mv $manifestfile ${manifestfile}_tmp
sort -u ${manifestfile}_tmp > ${manifestfile}
rm ${manifestfile}_tmp




echo "Deployment performed. Now starting Post-Deployment Step(s)" | tee -a $LOGFILE

# 1st post-deployment step - None Required
#echo "Post-Deployment Step 1 complete." | tee -a $LOGFILE


echo "Done." | tee -a $LOGFILE
