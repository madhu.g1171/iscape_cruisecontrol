<%@page import="net.sourceforge.cruisecontrol.*, net.sourceforge.cruisecontrol.chart.*"%>
<%@ taglib uri="/WEB-INF/lib/cewolf.jar" prefix="cewolf" %>
<%@ taglib uri="/WEB-INF/cruisecontrol-jsp11.tld" prefix="cruisecontrol"%>
<%
    String myProject = request.getPathInfo().substring(1);
	String truncProjName = "";
	String truncProjBranch = "";
	if (myProject.startsWith("webdev-")) {
		String[] splitName = myProject.split("-");
		truncProjName = splitName[1];
		truncProjBranch = splitName[2];
	}
%>
<cruisecontrol:buildInfo />

<table>
  <tr><td>Number of Build Attempts - <%=myProject%></td><td><%=build_info.size() %></td></tr>
  <tr><td>Number of Broken Builds</td><td><%=build_info.getNumBrokenBuilds() %></td></tr>
  <tr><td>Number of Successful Builds</td><td><%=build_info.getNumSuccessfulBuilds() %></td></tr>
</table>
<hr />
<table border="1">
  <tr>
  	<th>&nbsp;</th>
  	<th>Cobertura</th>
  	<th>Checkstyle</th>
  	<th>FindBugs</th>
	<th>Duplicate Lines</td>
  	<th>jDepend</th>
  </tr>
  <tr>
  	<th>Shared</th>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/cobertura-report-shared/index.html">Report</a><br/>
  	    <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/sharedchart-cobertura/index.html">Graph</a></td>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/checkstyle_report_shared.html">Report</a><br/>
  	    <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/sharedchart-checkstyle/index.html">Graph</a></td>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/findbugs_report_shared.html">Report</a><br/>
   	    <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/sharedchart-findbugs/index.html">Graph</a></td>
        <td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/cpd_report_shared.html">Report</a><br/>
            <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/sharedchart-cpd/index.html">Graph</a></td>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/jdependshared/index.html">Report</a><br/>
  	    <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/jdepend-shared.png">Dependencies Diagram (Large PNG File)</a></td>
  </tr>
  <tr>
    	<th>Refactored Shared</th>
    	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/cobertura-report-shared-refactored/index.html">Report</a><br/>
    	    <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/shared-refactoredchart-cobertura/index.html">Graph</a></td>
    	<td colspan=5">&nbsp;</td>
    </tr>
  <tr>
  	<th>Project (<%=truncProjName%>)</th>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/cobertura-report-<%=truncProjName%>/index.html">Report</a><br/>
            <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/<%=truncProjName%>chart-cobertura/index.html">Graph</a></td>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/checkstyle_report_<%=truncProjName%>.html">Report</a><br/>
            <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/<%=truncProjName%>chart-checkstyle/index.html">Graph</a></td>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/findbugs_report_<%=truncProjName%>.html">Report</a><br/>
  	    <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/<%=truncProjName%>chart-findbugs/index.html">Graph</a></td>
        <td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/cpd_report_<%=truncProjName%>.html">Report</a><br/>
            <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/<%=truncProjName%>chart-cpd/index.html">Graph</a></td>
  	<td><a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/jdepend<%=truncProjName%>/index.html">Report</a><br/>
            <a href="/cc-reports/webdev-<%=truncProjName%>-<%=truncProjBranch%>/jdepend-<%=truncProjName%>.png">Dependencies Diagram (Large PNG File)</a></td>
  </tr>
</table>

<hr />

<jsp:useBean id="pieData" class="net.sourceforge.cruisecontrol.chart.PieChartData" />
<cewolf:chart id="pie" title="Breakdown of build types" type="pie" >
    <cewolf:data>
        <cewolf:producer id="pieData">
          <cewolf:param name="buildInfo" value="<%=build_info%>" />
        </cewolf:producer>
    </cewolf:data>
</cewolf:chart>
<cewolf:img chartid="pie" renderer="cewolf" width="400" height="300"/>

<hr />
<jsp:useBean id="chartData" class="net.sourceforge.cruisecontrol.chart.TimeChartData" />
<cewolf:chart id="chart" title="Breakdown of build types" type="timeseries"  xaxislabel="date" yaxislabel="time">
    <cewolf:data>
        <cewolf:producer id="chartData">
          <cewolf:param name="buildInfo" value="<%=build_info%>" />
        </cewolf:producer>
    </cewolf:data>
    <cewolf:chartpostprocessor id="chartData" />
</cewolf:chart>
<cewolf:img chartid="chart" renderer="cewolf" width="400" height="300"/>

