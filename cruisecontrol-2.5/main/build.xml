<!--****************************************************************************
 * CruiseControl, a Continuous Integration Toolkit
 * Copyright (c) 2001, ThoughtWorks, Inc.
 * 651 W Washington Ave. Suite 600
 * Chicago, IL 60661 USA
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     + Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *     + Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *
 *     + Neither the name of ThoughtWorks, Inc., CruiseControl, nor the
 *       names of its contributors may be used to endorse or promote
 *       products derived from this software without specific prior
 *       written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 ****************************************************************************-->


<project name="CruiseControl" default="clean-all" basedir=".">
    <property environment="env"/>
    <property file="override.properties"/>

    <!-- define cc.version -->
    <property file="../build.properties"/>
    <!-- Use a local variable to make sure that a script that would read the 
         properties file would still be capable of overidding this one.
         Note: the release.xml overrides this property -->
    <property name="cc.release.label" value="${cc.version}"/>

    <property name="target" value="${basedir}/target"/>
    <property name="docs" value="${target}/docs"/>
    <property name="apidocs" value="${docs}/api"/>

    <property name="classes" value="${target}/classes"/>
    <property name="dist" value="dist"/>
    <property name="junit.results" value="${target}/test-results" />
    <property name="lib" value="${basedir}/lib"/>
    <property name="src" value="${basedir}/src"/>
    <property name="test" value="${basedir}/test"/>
    <property name="test.classes" value="${target}/test-classes" />
    <property name="tests" value="*Test"/>
    <property name="xsl" value="${basedir}/xsl"/>

    <property name="checkstyle.fail.on.violation" value="true"/>
    <property name="checkstyle.jar" value="${lib}/checkstyle-all-3.1.jar"/>

    <property name="clover.home" value="${env.CLOVER_HOME}"/>
    <property name="clover.jar" value="${clover.home}/lib/clover.jar"/>
    <property name="clover.results" value="${target}/clover-results"/>

    <property name="emma.results" value="${target}/emma-results"/>
    <property name="emma.stats" value="${emma.results}/coverage.emma"/>

    <path id='emma.classpath' >
        <pathelement location="${lib}/emma.jar"/>
        <pathelement location="${lib}/emma_ant.jar"/>
    </path>
    <taskdef resource="emma_ant.properties" classpathref="emma.classpath" />

    <path id="clover.classpath">
        <pathelement location="${clover.jar}"/>
    </path>
    
    <!-- ****************************************************
    Define the project classpath references.  Any jar found
    in lib directory will be included in CLASSPATH
    **************************************************** -->
    <path id="project.classpath" >
        <fileset dir="${lib}" includes="*.jar"/>
    	<!-- ant.jar is also needed to compile the ArtifactsPublisher,
    	     but is added automatically to the javac classpath -->
    </path>

    <path id="project.runtime.classpath" >
        <pathelement location="${test.classes}" />
        <path refid="project.classpath" />
        <pathelement location="${classes}" />
        <pathelement path="${java.class.path}"/>
        <path refid="clover.classpath"/>
    </path>

    <target name="checklabel">
        <fail unless="cc.release.label" message="label is not defined."/>
    </target>

    <target name="init" description="Setup build system" depends="checklabel">
        <mkdir dir="${classes}"/>
        <mkdir dir="${test.classes}"/>
        <mkdir dir="${target}/tmp"/>
        <mkdir dir="${dist}"/>
        <mkdir dir="${junit.results}"/>
        <mkdir dir="${clover.results}"/>
        <mkdir dir="${emma.results}"/>

        <available file="${lib}/starteam-sdk.jar" property="starteam-sdk-present"/>
        <available file="${lib}/STComm.jar" property="sametime-sdk-present"/>
    	<available file="${env.NANT_HOME}/NAnt.exe" property="nant-present" />
        <tstamp/>
        <property name="build.version" value="${cc.release.label}"/>
        <property name="build.version.info" value="Compiled on ${TODAY} ${TSTAMP}"/>
    </target>

    <target name="clean" description="Cleans out the build directories" >
       <delete quiet="yes">
            <fileset dir="${dist}"/>
       </delete>
       <delete dir="${target}"/>
    </target>

    <target name="compile" depends="init" unless="compile.skip" description="Compile source code">
        <javac destdir="${classes}" debug="true" deprecation="true" fork="true" source="1.3" target="1.3">
           <classpath refid="project.classpath"/>
           <src path="${src}"/>
           <exclude name="**/StarTeam*"/>
           <exclude name="**/Sametime*"/>
        </javac>
        <copy todir="${classes}">
            <fileset dir="${basedir}" includes="*.properties"/>
            <filterset>
                <filter token="VERSION" value="${build.version}" />
                <filter token="VERSION_INFO" value="${build.version.info}" />
            </filterset>
        </copy>
        <!-- copy resources like properties from the src-dir -->
        <copy todir="${classes}">
          <fileset dir="${src}" excludes="**/*.java"/>
        </copy>
    </target>

    <target name="compile-starteam" if="starteam-sdk-present" depends="init">
        <javac destdir="${classes}" debug="true" deprecation="true" fork="true" source="1.3" target="1.3">
            <classpath refid="project.classpath"/>
            <src path="${src}"/>
            <include name="**/StarTeam*"/>
        </javac>
    </target>

    <target name="compile-sametime" if="sametime-sdk-present" depends="init">
        <javac destdir="${classes}" debug="true" deprecation="true" fork="true" source="1.3" target="1.3">
            <classpath refid="project.classpath"/>
            <src path="${src}"/>
            <include name="**/Sametime*"/>
        </javac>
    </target>

    <target name="compile-test" depends="compile"
        description="Compile test code">

        <javac debug="on" deprecation="false" srcdir="${test}" fork="true" source="1.3" target="1.3"
            destdir="${test.classes}" excludes="**/*StarTeam*,**/*Sametime*"
            classpathref="project.runtime.classpath" />

        <copy todir="${test.classes}">
            <fileset dir="${test}" includes="**/*.xml"/>
            <fileset dir="${test}" includes="**/*.txt"/>
            <fileset dir="${test}" includes="**/*.jar"/>
            <fileset dir="${test}" includes="**/*.properties"/>
        </copy>
        <copy todir="${target}">
            <fileset dir="${test}" includes="testbuild.xml"/>
            <fileset dir="${test}" includes="test.build"/>
        </copy>
    </target>

    <target name="checkstyle" depends="init" description="Run CheckStyle on code">
        <taskdef resource="checkstyletask.properties" classpath="${checkstyle.jar}"/>
        <checkstyle config="${basedir}/checkstyle.xml" failOnViolation="${checkstyle.fail.on.violation}">
            <formatter type="plain"/>
            <formatter type="plain" tofile="${target}/checkstyleResults.txt"/>
            <fileset dir="${src}" includes="net/sourceforge/cruisecontrol/**/*.java"/>
            <fileset dir="${test}" includes="net/sourceforge/cruisecontrol/**/*.java"/>
        </checkstyle>
    </target>

    <target name="emma-instrument" depends="init, compile" >
        <emma>
          <instr instrpath="${classes}"
                 outdir="${classes}"
                 merge="yes"
                 metadatafile="${emma.results}/metadata.emma"
                 mode="overwrite"
          />
        </emma>
    </target>

    <target name="report-emma" depends="init, emma-instrument, test">
        <property name="emma.html.report" value="${emma.results}/coverage.html"/>
        <emma>
            <report sourcepath="${src}" >
                <fileset dir="${emma.results}" >
                  <include name="*.emma" />
                </fileset>

                <html outfile="${emma.html.report}" />
            </report>
        </emma>
        <echo message="View EMMA's HTML report at ${emma.html.report}"/>
    </target>

    <target name="report-coverage" depends="with-clover, clean, compile, compile-test, test, report-clover"
        description="Check test coverage"/>

    <target name="report-clover" depends="with-clover">
        <clover-report>
            <current outfile="${clover.results}" title="CruiseControl test coverage">
                <format type="html"/>
                <fileset dir="${src}"/>
            </current>   
        </clover-report>
    </target>

    <target name="with-clover">
        <fail message="CLOVER_HOME must be set!" unless="env.CLOVER_HOME"/>
        <fail message="Can't get Clover to work with Jikes..." 
            if="env.JIKESPATH"/>
        <taskdef resource="clovertasks" classpathref="clover.classpath"/>
        <clover-setup initstring="${clover.results}/cc_coverage.db"/>
    </target>

    <target name="check-duplication" if="env.SIMIAN_HOME" description="Check for duplicated code">
        <taskdef resource="simiantask.properties" classpath="${env.SIMIAN_HOME}/simian.jar"/>
        <!-- Simian is failing even if no duplicated lines? -->
        <simian failOnDuplication="false">
            <fileset dir="${src}" includes="**/*.java"/>
        </simian>        
    </target>

    <target name="test" depends="compile-test" unless="test.skip" description="Executes the unit tests">
        <junit fork="yes" forkmode="perBatch" haltonfailure="yes" printsummary="on" dir="${target}">
            <classpath >
                <path refid="project.runtime.classpath" />
            </classpath>
            <formatter type="brief" usefile="false"/>
            <formatter type="xml" />
            <batchtest todir="${junit.results}" >
                <fileset dir="${test.classes}"
                         includes="**/${tests}.class"
                         />
            </batchtest>
            <jvmarg value="-Demma.coverage.out.file=${emma.stats}" />
            <jvmarg value="-Demma.coverage.out.merge=true" />
        </junit>
    </target>

    <target name="test-one" depends="compile-test" description="Executes one unit test" if="testcase">
        <junit fork="yes" haltonfailure="yes" printsummary="on" dir="${target}">
            <classpath >
                <path refid="project.runtime.classpath" />
            </classpath>
            <formatter type="brief" usefile="false"/>
            <formatter type="xml" />
            <test name="${testcase}" todir="${junit.results}" />
        </junit>
    </target>

    <target name="test-starteam" depends="init, compile-starteam" if="starteam-sdk-present" unless="test.skip">
        <javac debug="on" deprecation="true" srcdir="${test}" destdir="${test.classes}" fork="true" source="1.3" target="1.3">
            <classpath refid="project.runtime.classpath"/>
            <src path="${test}"/>
            <include name="**/StarTeam*"/>
        </javac>
        <junit fork="yes" haltonfailure="yes" printsummary="on" >
            <classpath >
                <path refid="project.runtime.classpath" />
            </classpath>
            <formatter type="brief" usefile="false"/>
            <formatter type="xml" />
            <batchtest todir="${junit.results}" >
                <fileset dir="${test.classes}" includes="**/StarTeam*Test.class" />
            </batchtest>
        </junit>

    </target>

    <target name="test-sametime" depends="init, compile-sametime" if="sametime-sdk-present" unless="test.skip">
        <javac debug="on" deprecation="true" srcdir="${test}" destdir="${test.classes}" fork="true" source="1.3" target="1.3">
            <classpath refid="project.runtime.classpath"/>
            <src path="${test}"/>
            <include name="**/Sametime*"/>
        </javac>
        <junit fork="yes" haltonfailure="yes" printsummary="on" >
            <classpath >
                <path refid="project.runtime.classpath" />
            </classpath>
            <formatter type="brief" usefile="false"/>
            <formatter type="xml" />
            <batchtest todir="${junit.results}" >
                <fileset dir="${test.classes}" includes="**/Sametime*Test.class" />
            </batchtest>
        </junit>

    </target>

    <target name="jar" depends="compile">
        <jar jarfile="${dist}/cruisecontrol.jar" manifest="${basedir}/manifest.mf">
            <fileset dir="${classes}"/>
            <fileset dir="${xsl}"/>
            <manifest>
                <attribute name="Implementation-Version" value="${build.version} ${build.version.info}"/>  
            </manifest>
        </jar>
    </target>

    <target name="javadoc" depends="init,compile" description="Generate the javadocs">
        <mkdir dir="${docs}"/>
        <copy todir="${docs}">
            <fileset dir="docs"/>
        </copy>
        
        <mkdir dir="${apidocs}"/>
        <javadoc sourcepath="${src}" destdir="${apidocs}" packagenames="net.sourceforge.cruisecontrol.*"
            breakiterator="yes" failonerror="true">
            <classpath>
                <path refid="project.classpath"/>
                <pathelement location="${lib}/ant/ant.jar"/>
                <pathelement location="${lib}/ant/ant-launcher.jar"/>
                <pathelement location="${src}"/>
            </classpath>
        </javadoc>
    </target>

    <target name="clean-all" depends="clean, all" description="Performs a clean build, runs tests, and builds jar"/>
    
    <target name="all" depends="init,compile,compile-starteam,compile-sametime,checkstyle,check-duplication,test,test-starteam,test-sametime,jar" description="Performs a build, runs tests, and builds jar" />

    <target name="cvs-update" depends="init" if="localCopy">
        <cvs command="update -d -P" cvsroot=":pserver:anonymous@cvs.sourceforge.net:/cvsroot/cruisecontrol"
             package="cruisecontrol" dest="${localCopy}" />
    </target>

</project>
